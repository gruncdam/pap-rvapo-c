### ICE-V
The ICE-V design can be simulated using `./icestorm/simulate.sh`

To run on the board:
Ensure the Makefile in `integration/ice40/icestorm` contains correct path to your installation of Yosys and Python.
Said Python must have the `serial` package available.

Now you can use the Makefile. Following targets are of interest
- `make` will synthetise the design and create binary bitstream.
- `make prog` will program the FPGA, therefore running the design, synthetising if necessary.
- `make clean` needs to be used after every HDL edit, otherwise the design will not be resynthetised beacuse of my poor knowledge of Makefiles.  

Instruction memory contents for this FPGA are at present hardcoded in `rvapo/target_tb/rvapo_ice40_mem_impl.vhd`. Reportedly it is possible to change content of the ice40 RAM without resynthetising the whole design, but I have not been able to find sufficient documentation for this.

If you choose to edit this design, note that the icestorm synthesis tools are not as smart as Vivado. In particular, they are unlikely to handle any latches (the `-latches` paremeter of GHDL will only move the error further down the pipeline). 

In order to communicate with the soft core, use the pseudo RAM.
`python3.11 ../python/send_c3usb.py --ps_rd=ADDR LEN` to read (add `| xxd` to get hexdump)
`python3.11 ../python/send_c3usb.py --ps_wr=ADDR FILE` to write
### ICE-V generics
The ice40 version permits following configuration:

* SPI_BASE - All memory accesses above this address will be mapped into SPI PSRAM. Default is 0x1000.
* CLOCK_SRC - 0 for 32kHz internal oscillator (tested on FPGA), 1 for external clock (necessary for simulation, on FPGA there is 12MHz external clock, which is manually halved)
* debug - enable writing debug information (program counter, instruction, its result and RAM read data) to PSRAM ar addresses 0x1010+. 
* mem_delayed - Only used for compatibility. Ignore. 