library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
--use WORK.rvapo_pkg.all;
--use WORK.rvapo_wrappers.all;
entity InterruptController is
generic (
	debug_exec_delay:integer:=2
	);
port (
    clk				:in std_logic;
    rst				:in std_logic;

	instr			:out std_logic_vector(31 downto 0);
    pause			:out std_logic;
	issue			:out std_logic;

	irq				:in std_logic_vector(3 downto 0);
	irq_address		:in std_logic_vector(31 downto 0);
	trap			:in std_logic_vector(5 downto 0)
);
end;
architecture rtl of InterruptController is
	function make_instruction_I(
		opcode: std_logic_vector(6 downto 0);
		rd: std_logic_vector(4 downto 0);
		funct3: std_logic_vector(2 downto 0);
		rs1: std_logic_vector(4 downto 0);
		imm: std_logic_vector(11 downto 0)
	) return std_logic_vector is
		variable result: std_logic_vector(31 downto 0);
	begin
		result(6 downto 0) := opcode;
		result(11 downto 7) := rd;
		result(14 downto 12) := funct3;
		result(19 downto 15) := rs1;
		result(31 downto 20) := imm;
		return result;
	end function;

	function make_instruction_U(
		opcode: std_logic_vector(6 downto 0);
		rd: std_logic_vector(4 downto 0);
		imm: std_logic_vector(31 downto 12)
	) return std_logic_vector is
		variable result: std_logic_vector(31 downto 0);
	begin
		result(6 downto 0) := opcode;
		result(11 downto 7) := rd;
		result(31 downto 12) := imm;
		return result;
	end function;

	--constant SUBI_SP_SP_8:std_logic_vector(31 downto 0):=x"ff810113";--TODO: this is unnecessary, subtract more in sw handler
	--constant SW_RA_SP_0:std_logic_vector(31 downto 0):=x"00112023";
	--constant SW_A0_SP_4:std_logic_vector(31 downto 0):=x"00a12223";
	constant SW_RA_SP_M8:std_logic_vector(31 downto 0):=x"fe112c23";
	constant SW_A0_SP_M4:std_logic_vector(31 downto 0):=x"fea12e23";
	constant LW_RA_SP_8:std_logic_vector(31 downto 0):=x"ff812083";
	constant JALR_0_RA_0:std_logic_vector(31 downto 0):=x"00008067";
	constant ADDI:std_logic_vector(6 downto 0):="0010011";
	constant JALR:std_logic_vector(6 downto 0):="1100111";
	constant LUI:std_logic_vector(6 downto 0):="0110111";
	constant A0:std_logic_vector(4 downto 0):="01010";
	constant RA:std_logic_vector(4 downto 0):="00001";
	constant TRAP_CALL:std_logic_vector(5 downto 0):="000001";

	type State_t is(Idle,WaitEmpty,SaveRA,SaveA0,SetA0,JumpBase,Jump,Await,Untrap,RET,RAfix);

	signal iraddr_offset:std_logic_vector(7 downto 0);
	signal irq_reg:std_logic_vector(3 downto 0):=(others=>'0');
	signal irqaddr_reg:std_logic_vector(31 downto 0);
	signal waiter:std_logic_vector(3 downto 0);
	signal state:State_t;

	signal iraddr_upper:std_logic_vector(31 downto 12);
	signal iraddr_lower:std_logic_vector(11 downto 0);
begin
	main:process(clk)begin
		if rising_edge(clk) then
			if rst='1' then
				state<=Idle;
				pause<='0';
				issue<='0';
				waiter<=(others=>'0');
			else 
				case(state)is
					when Idle=>
						if irq_reg/=x"0" then
							state<=WaitEmpty;--StackPtr;
							waiter<=(others=>'0');
						--	issue<='1';
							pause<='1';
						else
							pause<='0';
							issue<='0';	
						end if;
					when WaitEmpty=>
						if to_integer(unsigned(waiter))=4 then
							state<=SaveRA;
							waiter<=(others=>'0');
							issue<='1';
						else
						--	issue<='0';
							waiter<=std_logic_vector(unsigned(waiter)+1);
						end if;	
					when SaveRA=>
						if to_integer(unsigned(waiter))=debug_exec_delay then
							state<=SaveA0;
							waiter<=(others=>'0');
							issue<='1';
						else
							issue<='0';
							waiter<=std_logic_vector(unsigned(waiter)+1);
						end if;		
					when SaveA0=>
						if to_integer(unsigned(waiter))=debug_exec_delay then
							state<=SetA0;
							waiter<=(others=>'0');
							issue<='1';
						else
							issue<='0';
							waiter<=std_logic_vector(unsigned(waiter)+1);
						end if;	
					when SetA0=>
						if to_integer(unsigned(waiter))=debug_exec_delay then
							state<=JumpBase;
							waiter<=(others=>'0');
							issue<='1';
						else
							issue<='0';
							waiter<=std_logic_vector(unsigned(waiter)+1);
						end if;		
					when JumpBase=>
						if to_integer(unsigned(waiter))=debug_exec_delay then
							state<=Jump;
							waiter<=(others=>'0');
							issue<='1';
						else
							issue<='0';
							waiter<=std_logic_vector(unsigned(waiter)+1);
						end if;		
					when Jump=>
						if to_integer(unsigned(waiter))=debug_exec_delay then
							state<=Await;
							waiter<=(others=>'0');
							pause<='0';
						--	issue<='0';
						else
							issue<='0';
							waiter<=std_logic_vector(unsigned(waiter)+1);
						end if;		
					when Await=>
						if trap=Trap_Call then
							state<=Untrap;
							pause<='1';
							issue<='1';
						end if;	
					when Untrap=>	
						if to_integer(unsigned(waiter))=debug_exec_delay then
							state<=RET;
							waiter<=(others=>'0');
							issue<='1'; 
						elsif to_integer(unsigned(waiter))=debug_exec_delay-1 then
							issue<='0'; 	
							waiter<=std_logic_vector(unsigned(waiter)+1);
						else
							issue<='1'; 
							waiter<=std_logic_vector(unsigned(waiter)+1);
						end if;			
					when RET=>--TODO shorter delay should suffice					
						if to_integer(unsigned(waiter))=debug_exec_delay+1 then
							state<=RAfix;
							waiter<=(others=>'0');
							issue<='1'; 
						else
							issue<='0'; 
							waiter<=std_logic_vector(unsigned(waiter)+1);
						end if;		
					when RAfix=>--TODO shorter delay should suffice
						issue<='0'; 
						if to_integer(unsigned(waiter))=debug_exec_delay then
							state<=Idle;
							waiter<=(others=>'0');
							pause<='0';
						else
							waiter<=std_logic_vector(unsigned(waiter)+1);
						end if;		
				end case;
			end if;
		end if;
	end process;

	instr_select:process(state,irqaddr_reg,iraddr_upper,iraddr_offset,iraddr_lower)
	begin
		if irqaddr_reg(11)='1' then
			iraddr_upper<=std_logic_vector(unsigned(irqaddr_reg(31 downto 12))+1);
			iraddr_lower<=std_logic_vector(unsigned(irqaddr_reg(11 downto 0))-4096);
		else
			iraddr_upper<=irqaddr_reg(31 downto 12);
			iraddr_lower<=irqaddr_reg(11 downto 0);	
		end if;

		case(state)is
			when SaveRA=>	instr<=SW_RA_SP_M8;
			when SaveA0=>	instr<=SW_A0_SP_M4;
			when SetA0=>	instr<=make_instruction_I(ADDI,A0,"000","00000",x"0"&iraddr_offset);
			when JumpBase=>	instr<=make_instruction_U(LUI,RA,iraddr_upper);
			when Jump=>		instr<=make_instruction_I(JALR,RA,"000",RA,iraddr_lower);
			when RET=>		instr<=JALR_0_RA_0;
			when RAfix=>	instr<=LW_RA_SP_8;
			when others=>	instr<=(others=>'0');
		end case;	
	end process;

	input_filter:process(clk)begin
		if rising_edge(clk) then
			if rst='1' then
				irq_reg<=(others=>'0');
			else
				if state=Idle then
					irq_reg<=irq;
					irqaddr_reg<=irq_address;
				elsif state=RAfix then
					irq_reg<=(others=>'0');	
				end if;
			end if;
		end if;
	end process;

	offset_calc:process(irq_reg)
	variable irqnum:std_logic_vector(1 downto 0);
	begin
		if irq_reg(0)='1' then irqnum:="00";
		elsif irq_reg(1)='1' then irqnum:="01";
		elsif irq_reg(2)='1' then irqnum:="10";
		else irqnum:="11";
		end if;
		iraddr_offset<="0000"&irqnum&"00";
	end process;
end;


