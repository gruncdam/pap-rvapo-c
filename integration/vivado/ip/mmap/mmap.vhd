library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
--use WORK.rvapo_pkg.all;
--use WORK.rvapo_wrappers.all;
entity MemoryMap is
generic (
--	AXI_BASE 	:std_logic_vector;
	debug      	:integer
	);
port (
    clk: in std_logic;
    rst: in std_logic;

	addr: in  std_logic_vector(31 downto 0);
	din: in  std_logic_vector(31 downto 0);
    dout: out  std_logic_vector(31 downto 0);
	we: in std_logic_vector(3 downto 0);
    req: in std_logic;
    delayed: out std_logic;
	stall: out std_logic;

	addr_dm: out std_logic_vector(31 downto 0);
	din_dm: out  std_logic_vector(31 downto 0);
	dout_dm: in  std_logic_vector(31 downto 0);
	en_dm: out std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
	we_dm: out std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
	delayed_dm: in std_logic;

	addr_fnc	: out std_logic_vector(4 downto 0);
	en_fnc      : out std_logic;
	din_fnc     : out std_logic_vector(31 downto 0);
	dout_fnc    : in std_logic_vector(31 downto 0);
	we_fnc		: out std_logic_vector(3 downto 0);
	
	 -- Global Signals
	M_AXI_ACLK          :   out std_logic;
	-- No reset
	-- Write address channel signals
	M_AXI_AWID          :   out std_logic_vector(5 DOWNTO 0);
	M_AXI_AWADDR        :   out std_logic_vector(31 downto 0);
	M_AXI_AWLEN         :   out std_logic_vector(7 downto 0);
	M_AXI_AWSIZE        :   out std_logic_vector(2 downto 0);
	M_AXI_AWBURST       :   out std_logic_vector(1 downto 0);
	M_AXI_AWLOCK        :   out std_logic_vector(1 downto 0);
	M_AXI_AWCACHE       :   out std_logic_vector(3 downto 0);
	M_AXI_AWPROT        :   out std_logic_vector(2 downto 0);
	M_AXI_AWQOS         :   out std_logic_vector(3 downto 0);
	M_AXI_AWUSER        :   out std_logic_vector(4 downto 0);
	M_AXI_AWVALID       :   out std_logic;
	M_AXI_AWREADY       :   in  std_logic;
	-- Write data channel signals
	M_AXI_WID           :   out std_logic_vector(5 downto 0);
	M_AXI_WDATA         :   out std_logic_vector(31 downto 0);--(2**axi_data_width_log2b - 1 downto 0);
	M_AXI_WSTRB         :   out std_logic_vector(3 downto 0);--(2**(axi_data_width_log2b - 3) - 1 downto 0);
	M_AXI_WLAST         :   out std_logic;
	M_AXI_WUSER        	:   out std_logic_vector(4 downto 0);
	M_AXI_WVALID        :   out std_logic;
	M_AXI_WREADY        :   in  std_logic;
	--  Write response channel signals
	M_AXI_BID           :   in  std_logic_vector(5 downto 0);
	M_AXI_BRESP         :   in  std_logic_vector(1 downto 0);
	M_AXI_BUSER        	:   in std_logic_vector(4 downto 0);
	M_AXI_BVALID        :   in  std_logic;
	M_AXI_BREADY        :   out std_logic;
	--  Read address channel signals
	M_AXI_ARID          :   out std_logic_vector(5 downto 0);
	M_AXI_ARADDR        :   out std_logic_vector(31 downto 0);
	M_AXI_ARLEN         :   out std_logic_vector(7 downto 0);
	M_AXI_ARSIZE        :   out std_logic_vector(2 downto 0);
	M_AXI_ARBURST       :   out std_logic_vector(1 downto 0);
	M_AXI_ARLOCK        :   out std_logic_vector(1 downto 0);
	M_AXI_ARCACHE       :   out std_logic_vector(3 downto 0);
	M_AXI_ARPROT        :   out std_logic_vector(2 downto 0);
	M_AXI_ARQOS         :   out std_logic_vector(3 downto 0);
	M_AXI_ARUSER        :   out std_logic_vector(4 downto 0);
	M_AXI_ARVALID       :   out std_logic;
	M_AXI_ARREADY       :   in  std_logic;
	-- Read data channel signals
	M_AXI_RID           :   in  std_logic_vector(5 downto 0);
	M_AXI_RDATA         :   in  std_logic_vector(31 downto 0);--(2**axi_data_width_log2b - 1 downto 0);
	M_AXI_RRESP         :   in  std_logic_vector(1 downto 0);
	M_AXI_RLAST         :   in  std_logic;
	M_AXI_RUSER        	:  	in std_logic_vector(4 downto 0);
	M_AXI_RVALID        :   in  std_logic;
	M_AXI_RREADY        :   out std_logic	
);
end;

architecture rtl of MemoryMap is
	component axim_test_v1_0 is
		generic (
			C_M00_AXI_TARGET_SLAVE_BASE_ADDR	: std_logic_vector	:= x"40000000";
			C_M00_AXI_BURST_LEN	: integer	:= 1;
			C_M00_AXI_ID_WIDTH	: integer	:= 6;
			C_M00_AXI_ADDR_WIDTH	: integer	:= 32;
			C_M00_AXI_DATA_WIDTH	: integer	:= 32;
			C_M00_AXI_AWUSER_WIDTH	: integer	:= 5;
			C_M00_AXI_ARUSER_WIDTH	: integer	:= 5;
			C_M00_AXI_WUSER_WIDTH	: integer	:= 5;
			C_M00_AXI_RUSER_WIDTH	: integer	:= 5;
			C_M00_AXI_BUSER_WIDTH	: integer	:= 5
		);
		port (
			data_in			:in std_logic_vector(C_M00_AXI_DATA_WIDTH-1 downto 0);
			data_out		:out std_logic_vector(C_M00_AXI_DATA_WIDTH-1 downto 0);
			addr			:in std_logic_vector(C_M00_AXI_ADDR_WIDTH-1 downto 0);
			web				:in std_logic_vector(C_M00_AXI_DATA_WIDTH/8-1 downto 0);
			m00_axi_init_axi_txn	: in std_logic;
			m00_axi_txn_done	: out std_logic;
			m00_axi_error	: out std_logic;
			m00_axi_aclk	: in std_logic;
			m00_axi_aresetn	: in std_logic;
			m00_axi_awid	: out std_logic_vector(C_M00_AXI_ID_WIDTH-1 downto 0);
			m00_axi_awaddr	: out std_logic_vector(C_M00_AXI_ADDR_WIDTH-1 downto 0);
			m00_axi_awlen	: out std_logic_vector(7 downto 0);
			m00_axi_awsize	: out std_logic_vector(2 downto 0);
			m00_axi_awburst	: out std_logic_vector(1 downto 0);
			m00_axi_awlock	: out std_logic;
			m00_axi_awcache	: out std_logic_vector(3 downto 0);
			m00_axi_awprot	: out std_logic_vector(2 downto 0);
			m00_axi_awqos	: out std_logic_vector(3 downto 0);
			m00_axi_awuser	: out std_logic_vector(C_M00_AXI_AWUSER_WIDTH-1 downto 0);
			m00_axi_awvalid	: out std_logic;
			m00_axi_awready	: in std_logic;
			m00_axi_wdata	: out std_logic_vector(C_M00_AXI_DATA_WIDTH-1 downto 0);
			m00_axi_wstrb	: out std_logic_vector(C_M00_AXI_DATA_WIDTH/8-1 downto 0);
			m00_axi_wlast	: out std_logic;
			m00_axi_wuser	: out std_logic_vector(C_M00_AXI_WUSER_WIDTH-1 downto 0);
			m00_axi_wvalid	: out std_logic;
			m00_axi_wready	: in std_logic;
			m00_axi_bid	: in std_logic_vector(C_M00_AXI_ID_WIDTH-1 downto 0);
			m00_axi_bresp	: in std_logic_vector(1 downto 0);
			m00_axi_buser	: in std_logic_vector(C_M00_AXI_BUSER_WIDTH-1 downto 0);
			m00_axi_bvalid	: in std_logic;
			m00_axi_bready	: out std_logic;
			m00_axi_arid	: out std_logic_vector(C_M00_AXI_ID_WIDTH-1 downto 0);
			m00_axi_araddr	: out std_logic_vector(C_M00_AXI_ADDR_WIDTH-1 downto 0);
			m00_axi_arlen	: out std_logic_vector(7 downto 0);
			m00_axi_arsize	: out std_logic_vector(2 downto 0);
			m00_axi_arburst	: out std_logic_vector(1 downto 0);
			m00_axi_arlock	: out std_logic;
			m00_axi_arcache	: out std_logic_vector(3 downto 0);
			m00_axi_arprot	: out std_logic_vector(2 downto 0);
			m00_axi_arqos	: out std_logic_vector(3 downto 0);
			m00_axi_aruser	: out std_logic_vector(C_M00_AXI_ARUSER_WIDTH-1 downto 0);
			m00_axi_arvalid	: out std_logic;
			m00_axi_arready	: in std_logic;
			m00_axi_rid	: in std_logic_vector(C_M00_AXI_ID_WIDTH-1 downto 0);
			m00_axi_rdata	: in std_logic_vector(C_M00_AXI_DATA_WIDTH-1 downto 0);
			m00_axi_rresp	: in std_logic_vector(1 downto 0);
			m00_axi_rlast	: in std_logic;
			m00_axi_ruser	: in std_logic_vector(C_M00_AXI_RUSER_WIDTH-1 downto 0);
			m00_axi_rvalid	: in std_logic;
			m00_axi_rready	: out std_logic
		);
	end component;
	constant AXI_BASE:std_logic_vector(31 downto 0):=x"00001000";

-- signal c_addr_dm:std_logic_vector(31 downto 0):=x"00000000";
--	signal c_din_dm:std_logic_vector(31 downto 0);
--	signal c_dout_dm:std_logic_vector(31 downto 0);
--	signal c_en_dm:std_logic;--! enable r/w/reset. Fixed '1' should suffice.		
--	signal c_rst_dm:std_logic;--! resets output. Fixed '0' should suffice.	
--	signal c_we_dm:std_logic_vector(3 downto 0);--! choose bytes to write (bitmap of 32b word)
--	signal c_delayed_dm:std_logic;
--	signal c_req_dm:std_logic;

--	signal c_din_hm:std_logic_vector(31 downto 0);

	signal axi_stall: std_logic := '0';--this must be initialised, because it will never be assigned again if no axi
	signal axi_data: std_logic_vector(31 downto 0);
	signal axi_start: std_logic := '0';
	signal axi_complete: std_logic := '0';
	signal axi_any: std_logic:='0';
	signal axi_err: std_logic;

	signal C_AXI_AWADDR        :    std_logic_vector(31 downto 0);
    signal C_AXI_WDATA         :    std_logic_vector(31 downto 0);
	signal C_AXI_BREADY         :    std_logic;
	signal C_AXI_WVALID         :    std_logic;
	signal C_AXI_AWVALID         :    std_logic;
	signal C_AXI_WSTRB         :    std_logic_vector(3 downto 0);
	signal C_AXI_WLAST         :    std_logic;

	signal nrst:std_logic;
	signal axi_region:std_logic:='0';
	signal fnc_region:std_logic:='0';
	signal ram_region:std_logic;
	signal fnc_region_delayer:std_logic:='0';
begin
	nrst<=not rst;

--	gen_debug_ctrl:if debug=1 generate
--		process(dsel,clk)begin
--			if dsel=x"09" then din_hm<=C_AXI_AWADDR;
--			elsif  dsel=x"0A" then din_hm<=C_AXI_WDATA;	--equals F2IM.pc
--			elsif  dsel=x"0B" then din_hm<=(others=>'0');
--			elsif  dsel=x"0C" then din_hm<=axi_complete&axi_any&axi_start&axi_err&		
--										axi_stall&"000"&
--										C_AXI_AWVALID&M_AXI_AWREADY&C_AXI_WVALID&M_AXI_WREADY&
--										M_AXI_BVALID&C_AXI_BREADY&"0"&C_AXI_WLAST&
--										x"000"&C_AXI_WSTRB;
--			elsif  dsel=x"0D" then din_hm<=(others=>'0');
--			elsif  dsel=x"0E" then din_hm<=(others=>'0');
--			elsif  dsel=x"0F" then din_hm<=(others=>'0');
--			else din_hm<=c_din_hm;
--			end if;
--		end process;
--	end generate gen_debug_ctrl;
	addr_fnc<=addr(6 downto 2);
	din_fnc<=din;
	en_fnc<=fnc_region and req;
	we_fnc<=we and (fnc_region&fnc_region&fnc_region&fnc_region);
	en_dm<='1';

	ram_region<='1' when addr(31 downto 13)=x"0000"&"000" else '0';

	M_AXI_ACLK<=clk;
--	axi_gen:if axi=1 and simple=0 generate
	fnc_region<='1' when addr(15 downto 13)="001" else '0';--unsigned(addr)>=unsigned(AXI_BASE) else '0';
	stall<=axi_stall;
	din_dm<=din;--can be sent directly as long as we is gated
	addr_dm<=addr;--can be sent directly because we assume stateless memory interface
	axi_region<='1' when addr(31 downto 16)/=x"0000" else '0';--unsigned(addr)>=unsigned(AXI_BASE) else '0';
	axi_stall<=axi_region and (axi_start or (not axi_complete));--stall cpu, address is in axi space and axi master has started work or is being requested to do so 
	we_dm<=we and (ram_region&ram_region&ram_region&ram_region);--gate ram write when axi
	axi_start<=axi_region and axi_complete and not axi_any and req;--request axi master to start working, because address is in axi space, but neither axi master nor this controller *registered* start of axi operation

	axi_ctrl: process(clk,axi_complete,axi_any,axi_start,axi_stall,axi_data,dout_dm) begin
		if rising_edge(clk) then			
			if axi_complete='0' then--or axir_complete='0' then
			--	c_delayed_dm<='0';	
				axi_any<='1';--this does not need to be clocked, but that is problem only if result returns in one cycle.
			else	
			--	c_delayed_dm<=delayed_dm;
				axi_any<='0';
			end if;

			fnc_region_delayer<=fnc_region;
		--	if axi_region='0' and req='1' then
		--		delayed<=delayed_dm;--if there is no axi, mem can supply its delay information (for the next cycle, when the bypass needs to be done)
		--	else
		--		delayed<='0';
		--	end if;
		end if;
	end process;
	delayed<=not axi_region and req and delayed_dm;
	--c_dout_dm<=axi_data when axi_any='1' else dout_dm;--the change of data source must be one clock after activating axi mode so that previous (delayed) memory read can finish normally 
	--dout<=axi_data when (axi_region and not req)='1' else dout_dm;--this is now achieved by switching only after req dropped (axi stalls, no bypass)

	dout_selector:process(axi_data,dout_dm,dout_fnc,axi_region,fnc_region_delayer,req)begin
		if (axi_region and not req)='1' then
			dout<=axi_data;
		elsif fnc_region_delayer='1' then
			dout<=dout_fnc;
		else 
			dout<=dout_dm;
		end if;
	end process;

	M_AXI_AWADDR<=C_AXI_AWADDR;
	M_AXI_WDATA<=C_AXI_WDATA;
	M_AXI_BREADY<=C_AXI_BREADY;
	M_AXI_WVALID<=C_AXI_WVALID;
	M_AXI_WSTRB<=C_AXI_WSTRB;
	M_AXI_WLAST<=C_AXI_WLAST;
	M_AXI_AWVALID<=C_AXI_AWVALID;
	axi_master: axim_test_v1_0
	port map(
		data_in				=>din,
		data_out			=>axi_data,
		addr				=>addr,
		web					=>we,

		m00_axi_init_axi_txn=>axi_start,
		m00_axi_txn_done	=>axi_complete,
		m00_axi_error		=>axi_err,
		m00_axi_aclk        =>clk,
		m00_axi_aresetn		=>nrst,
		
		m00_axi_awid	 =>M_AXI_AWID,
		m00_axi_awaddr	 =>C_AXI_AWADDR,
		m00_axi_awlen	 =>M_AXI_AWLEN,
		m00_axi_awsize	 =>M_AXI_AWSIZE,
		m00_axi_awburst	 =>M_AXI_AWBURST,
		m00_axi_awlock	 =>M_AXI_AWLOCK(0),
		m00_axi_awcache	 =>M_AXI_AWCACHE,
		m00_axi_awprot	 =>M_AXI_AWPROT,
		m00_axi_awqos	 =>M_AXI_AWQOS,
		m00_axi_awuser	 =>M_AXI_AWUSER,
		m00_axi_awvalid	 =>C_AXI_AWVALID,
		m00_axi_awready	 =>M_AXI_AWREADY,
		m00_axi_wdata	 =>C_AXI_WDATA,
		m00_axi_wstrb	 =>C_AXI_WSTRB,
		m00_axi_wlast	 =>C_AXI_WLAST,
		m00_axi_wuser	 =>M_AXI_WUSER,
		m00_axi_wvalid	 =>C_AXI_WVALID,
		m00_axi_wready	 =>M_AXI_WREADY,
		m00_axi_bid	 	 =>M_AXI_BID,
		m00_axi_bresp	 =>M_AXI_BRESP,
		m00_axi_buser	 =>M_AXI_BUSER,
		m00_axi_bvalid	 =>M_AXI_BVALID,
		m00_axi_bready	 =>C_AXI_BREADY,
		m00_axi_arid	 =>M_AXI_ARID,
		m00_axi_araddr	 =>M_AXI_ARADDR,
		m00_axi_arlen	 =>M_AXI_ARLEN,
		m00_axi_arsize	 =>M_AXI_ARSIZE,
		m00_axi_arburst	 =>M_AXI_ARBURST,
		m00_axi_arlock	 =>M_AXI_ARLOCK(0),
		m00_axi_arcache	 =>M_AXI_ARCACHE,
		m00_axi_arprot	 =>M_AXI_ARPROT,
		m00_axi_arqos	 =>M_AXI_ARQOS,
		m00_axi_aruser	 =>M_AXI_ARUSER,
		m00_axi_arvalid	 =>M_AXI_ARVALID,
		m00_axi_arready	 =>M_AXI_ARREADY,
		m00_axi_rid	 	 =>M_AXI_RID,
		m00_axi_rdata	 =>M_AXI_RDATA,
		m00_axi_rresp	 =>M_AXI_RRESP,
		m00_axi_rlast	 =>M_AXI_RLAST,
		m00_axi_ruser	 =>M_AXI_RUSER,
		m00_axi_rvalid	 =>M_AXI_RVALID,
		m00_axi_rready	 =>M_AXI_RREADY
		);
--	end generate axi_gen;    
--	shortcut_gen:if axi=0 or simple=1 generate
--		c_dout_dm<=dout_dm;
--		c_delayed_dm<=delayed_dm;
--		we_dm<=c_we_dm;
--	end generate shortcut_gen;
end;


