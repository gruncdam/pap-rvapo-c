library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity pmsm_tb is
generic (
        AXI_BASE 	:std_logic_vector:=x"00001000";
    	axi      	:integer:=1;
		simple		:integer:=0;
		debug      	:integer:=0; 
		unalign		:integer:=1;
	    mem_delayed :std_logic:='1';
		enable_M	:integer:=1
        );
end entity;

architecture tb of pmsm_tb is
	component pmsm_3pmdrv1_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 6;

		-- Parameters of Axi Slave Bus Interface S_AXI_INTR
		C_S_AXI_INTR_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_INTR_ADDR_WIDTH	: integer	:= 5;
		C_NUM_OF_INTR	: integer	:= 1;
		C_INTR_SENSITIVITY	: std_logic_vector	:= x"FFFFFFFF";
		C_INTR_ACTIVE_STATE	: std_logic_vector	:= x"FFFFFFFF";
		C_IRQ_SENSITIVITY	: integer	:= 1;
		C_IRQ_ACTIVE_STATE	: integer	:= 1
	);
	port (
		-- Users to add ports here
        PWM_OUT : out std_logic_vector(1 to 3);
        PWM_SHDN : out std_logic_vector(1 to 3);
        PWM_STAT : in std_logic_vector(1 to 3);

        HAL_SENS : in std_logic_vector(1 to 3);

        ADC_SCLK: out std_logic;
        ADC_SCS: out std_logic;
        ADC_MOSI: out std_logic;
        ADC_MISO: in std_logic;

        PWR_STAT: in std_logic;

        IRC_CHA: in std_logic;
        IRC_CHB: in std_logic;
        IRC_IDX: in std_logic;
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic;

		-- Ports of Axi Slave Bus Interface S_AXI_INTR
		s_axi_intr_aclk	: in std_logic;
		s_axi_intr_aresetn	: in std_logic;
		s_axi_intr_awaddr	: in std_logic_vector(C_S_AXI_INTR_ADDR_WIDTH-1 downto 0);
		s_axi_intr_awprot	: in std_logic_vector(2 downto 0);
		s_axi_intr_awvalid	: in std_logic;
		s_axi_intr_awready	: out std_logic;
		s_axi_intr_wdata	: in std_logic_vector(C_S_AXI_INTR_DATA_WIDTH-1 downto 0);
		s_axi_intr_wstrb	: in std_logic_vector((C_S_AXI_INTR_DATA_WIDTH/8)-1 downto 0);
		s_axi_intr_wvalid	: in std_logic;
		s_axi_intr_wready	: out std_logic;
		s_axi_intr_bresp	: out std_logic_vector(1 downto 0);
		s_axi_intr_bvalid	: out std_logic;
		s_axi_intr_bready	: in std_logic;
		s_axi_intr_araddr	: in std_logic_vector(C_S_AXI_INTR_ADDR_WIDTH-1 downto 0);
		s_axi_intr_arprot	: in std_logic_vector(2 downto 0);
		s_axi_intr_arvalid	: in std_logic;
		s_axi_intr_arready	: out std_logic;
		s_axi_intr_rdata	: out std_logic_vector(C_S_AXI_INTR_DATA_WIDTH-1 downto 0);
		s_axi_intr_rresp	: out std_logic_vector(1 downto 0);
		s_axi_intr_rvalid	: out std_logic;
		s_axi_intr_rready	: in std_logic;
		irq	: out std_logic
	);
	end component;

	signal M_AXI_ACLK          :    std_logic;
	
	signal M_AXI_AWID          :    std_logic_vector(5 DOWNTO 0);
    signal M_AXI_AWADDR        :    std_logic_vector(31 downto 0);
    signal M_AXI_AWLEN         :    std_logic_vector(7 downto 0);
    signal M_AXI_AWSIZE        :    std_logic_vector(2 downto 0);
    signal M_AXI_AWBURST       :    std_logic_vector(1 downto 0);
    signal M_AXI_AWLOCK        :    std_logic_vector(1 downto 0);
    signal M_AXI_AWCACHE       :    std_logic_vector(3 downto 0);
    signal M_AXI_AWPROT        :    std_logic_vector(2 downto 0);
    signal M_AXI_AWQOS         :    std_logic_vector(3 downto 0);
    signal M_AXI_AWUSER        :    std_logic_vector(4 downto 0);
    signal M_AXI_AWVALID       :    std_logic;
	signal M_AXI_AWREADY	   :    std_logic;		
	
	signal M_AXI_WID           :    std_logic_vector(5 downto 0);
    signal M_AXI_WDATA         :    std_logic_vector(31 downto 0);
    signal M_AXI_WSTRB         :    std_logic_vector(3 downto 0);
    signal M_AXI_WLAST         :    std_logic;
    signal M_AXI_WVALID        :    std_logic;
	signal M_AXI_WREADY		   :    std_logic;
		
	signal M_AXI_BRESP		   :	std_logic_vector(1 downto 0);
	signal M_AXI_BVALID		   :    std_logic;
	signal M_AXI_BREADY        :    std_logic;
	signal M_AXI_BUSER         :    std_logic_vector(4 downto 0);

	signal M_AXI_ARID          :    std_logic_vector(5 downto 0);
    signal M_AXI_ARADDR        :    std_logic_vector(31 downto 0);
    signal M_AXI_ARLEN         :    std_logic_vector(7 downto 0);
    signal M_AXI_ARSIZE        :    std_logic_vector(2 downto 0);
    signal M_AXI_ARBURST       :    std_logic_vector(1 downto 0);
    signal M_AXI_ARLOCK        :    std_logic_vector(1 downto 0);
    signal M_AXI_ARCACHE       :    std_logic_vector(3 downto 0);
    signal M_AXI_ARPROT        :    std_logic_vector(2 downto 0);
    signal M_AXI_ARQOS         :    std_logic_vector(3 downto 0);
    signal M_AXI_ARUSER        :    std_logic_vector(4 downto 0);
    signal M_AXI_ARVALID       :    std_logic;
	signal M_AXI_ARREADY	   :	std_logic;
	
	signal M_AXI_RDATA         :    std_logic_vector(31 downto 0);--(2**axi_data_width_log2b - 1 downto 0);
	signal M_AXI_RRESP         :    std_logic_vector(1 downto 0);
	signal M_AXI_RVALID        :    std_logic;
	signal M_AXI_RREADY		   :	std_logic;		
    signal M_AXI_RUSER         :    std_logic_vector(4 downto 0);

	signal ADC_MISO:std_logic:='0';
	signal ADC_SCLK:std_logic:='0';
	signal ADC_SCLK_old:std_logic:='0';
	signal IRC_CHA:std_logic:='0';
	signal IRC_CHB:std_logic:='1';
	signal IRC_shiftB:std_logic:='1';
	signal IRC_div:std_logic_vector(3 downto 0):=(others=>'0');
	signal IRC_IDX:std_logic:='0';

	signal clock:std_logic:='0';
	signal resetn:std_logic:='0';
	signal done:std_logic:='0';

	signal ADC_counter:std_logic_vector(11 downto 0):=x"811";
	signal ADC_shift:std_logic_vector(15 downto 0):=x"0300";
begin
	clock <= not clock after 10 ns when done /= '1' else '0';

	ADC_MISO<=ADC_shift(15);

	data:process(clock)begin
		if rising_edge(clock) and resetn='1' then
			IRC_div<=std_logic_vector(unsigned(IRC_div)+1);
			if IRC_div=x"0" then
				IRC_CHA<=not IRC_CHA;--just feed the irc whatever to see that it does something
				IRC_shiftB<=not IRC_shiftB;
			end if;	
			IRC_CHB<=IRC_shiftB;

			ADC_SCLK_old<=ADC_SCLK;

			if ADC_SCLK='1' and ADC_SCLK_old='0' then
				if ADC_shift=x"C000" then
					ADC_shift<="00"&ADC_counter&"11";
					ADC_counter<=std_logic_vector(unsigned(ADC_counter)+1);
				else
					ADC_shift<=std_logic_vector(shift_left(unsigned(ADC_shift),1));
				end if;
			end if;
		end if;
	end process;

	main:process begin
		wait for 50 ns;
		resetn<='1';
		wait for 401000 ns;
		done<='1';
		wait;
	end process;

   	eut:pmsm_3pmdrv1_v1_0 
--	generic map(
--		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
--		C_S00_AXI_ADDR_WIDTH	: integer	:= 6;

		-- Parameters of Axi Slave Bus Interface S_AXI_INTR
--		C_S_AXI_INTR_DATA_WIDTH	: integer	:= 32;
--		C_S_AXI_INTR_ADDR_WIDTH	: integer	:= 5;
--		C_NUM_OF_INTR	: integer	:= 1;
--		C_INTR_SENSITIVITY	: std_logic_vector	:= x"FFFFFFFF";
--		C_INTR_ACTIVE_STATE	: std_logic_vector	:= x"FFFFFFFF";
--		C_IRQ_SENSITIVITY	: integer	:= 1;
--		C_IRQ_ACTIVE_STATE	: integer	:= 1
--		)
	port map(
		-- Users to add ports here
        PWM_OUT 		=>open,
        PWM_SHDN 		=>open,
        PWM_STAT 		=>"100",

        HAL_SENS 		=>"010",

        ADC_SCLK		=>ADC_SCLK,
        ADC_SCS			=>open,
        ADC_MOSI		=>open,
        ADC_MISO 		=>ADC_MISO,

        PWR_STAT		=>'0',

        IRC_CHA			=>IRC_CHA,
        IRC_CHB			=>IRC_CHB,
        IRC_IDX			=>IRC_IDX,
		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	=>clock,
		s00_axi_aresetn	=>resetn,
		s00_axi_awaddr	=>(others=>'0'),
		s00_axi_awprot	=>(others=>'0'),
		s00_axi_awvalid	=>'0',
		s00_axi_awready	=>open,
		s00_axi_wdata	=>(others=>'0'),
		s00_axi_wstrb	=>(others=>'0'),
		s00_axi_wvalid	=>'0',
		s00_axi_wready	=>open,
		s00_axi_bresp	=>open,
		s00_axi_bvalid	=>open,
		s00_axi_bready	=>'0',
		s00_axi_araddr	=>(others=>'0'),
		s00_axi_arprot	=>(others=>'0'),
		s00_axi_arvalid	=>'0',
		s00_axi_arready	=>open,
		s00_axi_rdata	=>open,
		s00_axi_rresp	=>open,
		s00_axi_rvalid	=>open,
		s00_axi_rready	=>'0',

		-- Ports of Axi Slave Bus Interface S_AXI_INTR
		s_axi_intr_aclk		=>clock,
		s_axi_intr_aresetn	=>resetn,
		s_axi_intr_awaddr	=>(others=>'0'),
		s_axi_intr_awprot	=>(others=>'0'),
		s_axi_intr_awvalid	=>'0',
		s_axi_intr_awready	=>open,
		s_axi_intr_wdata	=>(others=>'0'),
		s_axi_intr_wstrb	=>(others=>'0'),
		s_axi_intr_wvalid	=>'0',
		s_axi_intr_wready	=>open,
		s_axi_intr_bresp	=>open,
		s_axi_intr_bvalid	=>open,
		s_axi_intr_bready	=>'0',
		s_axi_intr_araddr	=>(others=>'0'),
		s_axi_intr_arprot	=>(others=>'0'),
		s_axi_intr_arvalid	=>'0',
		s_axi_intr_arready	=>open,
		s_axi_intr_rdata	=>open,
		s_axi_intr_rresp	=>open,
		s_axi_intr_rvalid	=>open,
		s_axi_intr_rready	=>'0',
		irq	=>open
	);
end architecture;
