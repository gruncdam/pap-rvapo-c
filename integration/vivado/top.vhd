library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_hdl is
    port (
        DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
        DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
        DDR_cas_n : inout STD_LOGIC;
        DDR_ck_n : inout STD_LOGIC;
        DDR_ck_p : inout STD_LOGIC;
        DDR_cke : inout STD_LOGIC;
        DDR_cs_n : inout STD_LOGIC;
        DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
        DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
        DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
        DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
        DDR_odt : inout STD_LOGIC;
        DDR_ras_n : inout STD_LOGIC;
        DDR_reset_n : inout STD_LOGIC;
        DDR_we_n : inout STD_LOGIC;
        FIXED_IO_ddr_vrn : inout STD_LOGIC;
        FIXED_IO_ddr_vrp : inout STD_LOGIC;
        FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
        FIXED_IO_ps_clk : inout STD_LOGIC;
        FIXED_IO_ps_porb : inout STD_LOGIC;
        FIXED_IO_ps_srstb : inout STD_LOGIC;
        FPGA_IO_A: inout std_logic_vector(10 downto 1);
        FPGA_IO_B: inout std_logic_vector(28 downto 13);
        FPGA_IO_C: inout std_logic_vector(40 downto 31)
    );
end entity;

architecture structure of top_hdl is
signal test_port:std_logic_vector(7 downto 0);
signal b :  std_logic_vector(7 downto 0);
signal g :  std_logic_vector(7 downto 0);
signal hsync :  STD_LOGIC;
signal r :  std_logic_vector(7 downto 0);
signal vsync :  STD_LOGIC;
signal mainclk :  STD_LOGIC;
signal mainrst :  STD_LOGIC;

signal ADC_MISO : STD_LOGIC;
signal ADC_MOSI : STD_LOGIC;
signal ADC_SCLK : STD_LOGIC;
signal ADC_SCS : STD_LOGIC;
signal HAL_SENS : STD_LOGIC_VECTOR ( 1 to 3 );
signal IRC_CHA : STD_LOGIC;
signal IRC_CHB : STD_LOGIC;
signal IRC_IDX : STD_LOGIC;
signal PWM_OUT : STD_LOGIC_VECTOR ( 1 to 3 );
signal PWM_SHDN : STD_LOGIC_VECTOR ( 1 to 3 );
signal PWM_STAT : STD_LOGIC_VECTOR ( 1 to 3 );
signal PWR_STAT : STD_LOGIC;
begin
    
 --   FPGA_IO_A(8 downto 1)<=test_port;
 --   FPGA_IO_A(10 downto 9)<="";
 --   FPGA_IO_A(1)<='0';
 --   FPGA_IO_A(2)<='1';    
--	FPGA_IO_A(3)<=r(6);
 --   FPGA_IO_A(4)<=r(7);    
--	FPGA_IO_A(5)<='0';
 --   FPGA_IO_A(6)<=r(7);    
--	FPGA_IO_A(7)<='0';
--    FPGA_IO_A(8)<='1';   
    
--    FPGA_IO_B(13)<=g(6);
--    FPGA_IO_B(14)<=g(7);   
    
 --   FPGA_IO_B(22)<=hsync;
--    FPGA_IO_B(21)<=vsync;    
    
--    FPGA_IO_B(26)<=mainrst;
--    FPGA_IO_B(27)<=test_port(0);
--    FPGA_IO_B(28)<=mainclk;
        
--    FPGA_IO_C(31)<=b(6);
--    FPGA_IO_C(32)<=b(7);   
    FPGA_IO_A(1) <= PWM_SHDN(1);
    FPGA_IO_A(3) <= PWM_OUT(1);
    FPGA_IO_A(5) <= PWM_SHDN(2);
    FPGA_IO_A(7) <= PWM_OUT(2);
    FPGA_IO_A(9) <= PWM_SHDN(3);
    FPGA_IO_B(13) <= PWM_OUT(3);

    PWM_STAT(1) <= FPGA_IO_B(15);
    PWM_STAT(2) <= FPGA_IO_B(17);
    PWM_STAT(3) <= FPGA_IO_B(19);

    PWR_STAT <= FPGA_IO_B(21);

    ADC_MISO <= FPGA_IO_B(22);
    FPGA_IO_B(23) <= ADC_MOSI;
    FPGA_IO_B(24) <= ADC_SCLK;
    FPGA_IO_B(25) <= ADC_SCS;

    IRC_CHA <= FPGA_IO_B(27);
    IRC_CHB <= FPGA_IO_C(31);
    IRC_IDX <= FPGA_IO_C(33);

    HAL_SENS(1) <= FPGA_IO_C(35);
    HAL_SENS(2) <= FPGA_IO_C(37);
    HAL_SENS(3) <= FPGA_IO_C(39);

    i_top: entity work.top_wrapper
    port map (
        ADC_MISO=>ADC_MISO, 
          ADC_MOSI =>ADC_MOSI,
          ADC_SCLK =>ADC_SCLK, 
          ADC_SCS =>       ADC_SCS,
        DDR_addr(14 downto 0)     => DDR_addr(14 downto 0),
        DDR_ba(2 downto 0)        => DDR_ba(2 downto 0),
        DDR_cas_n                 => DDR_cas_n,
        DDR_ck_n                  => DDR_ck_n,
        DDR_ck_p                  => DDR_ck_p,
        DDR_cke                   => DDR_cke,
        DDR_cs_n                  => DDR_cs_n,
        DDR_dm(3 downto 0)        => DDR_dm(3 downto 0),
        DDR_dq(31 downto 0)       => DDR_dq(31 downto 0),
        DDR_dqs_n(3 downto 0)     => DDR_dqs_n(3 downto 0),
        DDR_dqs_p(3 downto 0)     => DDR_dqs_p(3 downto 0),
        DDR_odt                   => DDR_odt,
        DDR_ras_n                 => DDR_ras_n,
        DDR_reset_n               => DDR_reset_n,
        DDR_we_n                  => DDR_we_n,
        FIXED_IO_ddr_vrn          => FIXED_IO_ddr_vrn,
        FIXED_IO_ddr_vrp          => FIXED_IO_ddr_vrp,
        FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
        FIXED_IO_ps_clk           => FIXED_IO_ps_clk,
        FIXED_IO_ps_porb          => FIXED_IO_ps_porb,
        FIXED_IO_ps_srstb         => FIXED_IO_ps_srstb,
        FCLK_CLK0_0               => mainclk,
        FCLK_RESET0_N_0           => mainrst,
         HAL_SENS => HAL_SENS,
               IRC_CHA =>IRC_CHA,  
               IRC_CHB =>IRC_CHB,  
               IRC_IDX =>IRC_IDX,  
               PWM_OUT =>PWM_OUT,  
               PWM_SHDN=>PWM_SHDN, 
               PWM_STAT=>PWM_STAT, 
               PWR_STAT=>PWR_STAT, 
        b=>b,
        g=>g,
        hsync=>hsync,
        r=>r,
        test_port               =>test_port,
        vsync=>vsync
    );
end architecture;
