--Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2018.2.2 (lin64) Build 2348494 Mon Oct  1 18:25:39 MDT 2018
--Date        : Sat Dec  2 09:52:55 2023
--Host        : achmet-linux running 64-bit Ubuntu 22.04.2 LTS
--Command     : generate_target top_wrapper.bd
--Design      : top_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity top_wrapper is
  port (
    ADC_MISO : in STD_LOGIC;
      ADC_MOSI : out STD_LOGIC;
      ADC_SCLK : out STD_LOGIC;
      ADC_SCS : out STD_LOGIC;
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    FCLK_CLK0_0 : out STD_LOGIC;
    FCLK_RESET0_N_0 : out STD_LOGIC;
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
      HAL_SENS : in STD_LOGIC_VECTOR ( 1 to 3 );
      IRC_CHA : in STD_LOGIC;
      IRC_CHB : in STD_LOGIC;
      IRC_IDX : in STD_LOGIC;
      PWM_OUT : out STD_LOGIC_VECTOR ( 1 to 3 );
      PWM_SHDN : out STD_LOGIC_VECTOR ( 1 to 3 );
      PWM_STAT : in STD_LOGIC_VECTOR ( 1 to 3 );
      PWR_STAT : in STD_LOGIC;
    b : out STD_LOGIC_VECTOR ( 7 downto 0 );
    g : out STD_LOGIC_VECTOR ( 7 downto 0 );
    hsync : out STD_LOGIC;
    r : out STD_LOGIC_VECTOR ( 7 downto 0 );
    test_port : out STD_LOGIC_VECTOR ( 7 downto 0 );
    vsync : out STD_LOGIC
  );
end top_wrapper;

architecture STRUCTURE of top_wrapper is
  component top is
  port (
   ADC_MISO : in STD_LOGIC;
       ADC_MOSI : out STD_LOGIC;
       ADC_SCLK : out STD_LOGIC;
       ADC_SCS : out STD_LOGIC;
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FCLK_CLK0_0 : out STD_LOGIC;
    FCLK_RESET0_N_0 : out STD_LOGIC;
    HAL_SENS : in STD_LOGIC_VECTOR ( 1 to 3 );
          IRC_CHA : in STD_LOGIC;
          IRC_CHB : in STD_LOGIC;
          IRC_IDX : in STD_LOGIC;
          PWM_OUT : out STD_LOGIC_VECTOR ( 1 to 3 );
          PWM_SHDN : out STD_LOGIC_VECTOR ( 1 to 3 );
          PWM_STAT : in STD_LOGIC_VECTOR ( 1 to 3 );
          PWR_STAT : in STD_LOGIC;
    b : out STD_LOGIC_VECTOR ( 7 downto 0 );
    g : out STD_LOGIC_VECTOR ( 7 downto 0 );
    hsync : out STD_LOGIC;
    r : out STD_LOGIC_VECTOR ( 7 downto 0 );
    test_port : out STD_LOGIC_VECTOR ( 7 downto 0 );
    vsync : out STD_LOGIC
  );
  end component top;
begin
top_i: component top
     port map (
      ADC_MISO=>ADC_MISO, 
      ADC_MOSI =>ADC_MOSI,
      ADC_SCLK =>ADC_SCLK, 
      ADC_SCS =>       ADC_SCS,
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      FCLK_CLK0_0 => FCLK_CLK0_0,
      FCLK_RESET0_N_0 => FCLK_RESET0_N_0,
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
        HAL_SENS => HAL_SENS,
        IRC_CHA =>IRC_CHA,  
        IRC_CHB =>IRC_CHB,  
        IRC_IDX =>IRC_IDX,  
        PWM_OUT =>PWM_OUT,  
        PWM_SHDN=>PWM_SHDN, 
        PWM_STAT=>PWM_STAT, 
        PWR_STAT=>PWR_STAT, 
      b => b,
      g => g,
      hsync => hsync,
      r => r,
      test_port(7 downto 0) => test_port(7 downto 0),
      vsync => vsync
    );
end STRUCTURE;
