library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use WORK.rvapo_pkg.all;

entity ArithmeticLogicUnit is
	generic(
		enable_M	:integer:=0
		);
	port (
		clock: in Clock_t;
		from_e: in Intcon_E2ALU;
		to_e: out Intcon_ALU2E
	);
end;

architecture rtl of ArithmeticLogicUnit is
	signal tomul:Intcon_ALU2MUL;
	signal frommul:Intcon_MUL2ALU;
	signal tob:Intcon_ALU2B;
	signal fromb:Intcon_B2ALU;
--	signal mul_result: BusWidth_t;
	signal result: BusWidth_t;
--	signal aluwait;
	signal asign:std_logic:='0';
	signal bsign:std_logic:='0';
	signal ismul:std_logic:='0';--avoid undefined when no M
	signal bsrcA:BusWidth_t;
	signal newsignal:std_logic:='0';
begin
M_enabled:if enable_M=1 generate
	ismul<='1' when from_e.alu_control=AluControl_MUL else '0';
	inst_mul:Multiply port map(
		clock=>clock,
		from_alu=>tomul,
		to_alu=>frommul
	);
	asign<='1' when from_e.multype=Mul_Signed or from_e.multype=Mul_Mixed or from_e.multype=Mul_Div or from_e.multype=Mul_Rem or from_e.multype=Mul_High  else '0';
	bsign<='1' when from_e.multype=Mul_Signed or from_e.multype=Mul_Div or from_e.multype=Mul_Rem or from_e.multype=Mul_High else '0';
	tomul<=(
		from_e.fresh,
		from_e.multype,
		ismul and from_e.fresh,--not frommul.done,
		asign,
		bsign,
		from_e.srcA,
		from_e.srcB,
		fromb.result	
		);

	inst_bit:Bitmanip port map(
		from_alu=>tob,
		to_alu=>fromb
		);
	tob<=(bsrcA,from_e.srcB);
	bsrcA<=frommul.clz_request when ismul='1' else from_e.srcA;
end generate M_enabled;
	to_e.alu_wait<=(not frommul.done or from_e.fresh) and ismul;--always 0 when no mul

	to_e.alu_out <= result;
	to_e.alu_flag.zero <= '1' when result = BUSWIDTH_ZERO else '0';

	process (from_e,frommul,fromb)
		variable srcA: BusWidth_t;
		variable srcB: BusWidth_t;
		variable tmp:BusWidth_t:=(others=>'0');
		variable result_v:BusWidth_t:=(others=>'0');
		variable subtract:std_logic_vector(32 downto 0);
		variable shifter_in:std_logic_vector(31 downto 0);
		variable shifted:std_logic_vector(32 downto 0);
		variable arith:std_logic:='0';
	begin
		srcA := from_e.srcA;
		srcB := from_e.srcB;
		subtract:=std_logic_vector(unsigned("1"&srcA)-unsigned("1"&srcB));
		if from_e.alu_control=AluControl_SLL then
			shifter_in:=reverse_vector(srcA);
		else
			shifter_in:=srcA;
		end if;
		if from_e.alu_control=AluControl_SRA then
			arith:='1';
		else
			arith:='0';	
		end if;
		shifted:=std_logic_vector(shift_right(signed(arith&shifter_in),to_integer(unsigned(from_e.srcB(4 downto 0)))));
		case from_e.alu_control is
			when AluControl_ADD =>
				result <= std_logic_vector(
					unsigned(srcA) + unsigned(srcB)
				);
			
			when AluControl_SUB =>
				result <= subtract(31 downto 0);

			when AluControl_SLL =>
				result <= reverse_vector(shifted(31 downto 0));

			when AluControl_SLT =>
			--	if signed(srcA) < signed(srcB) then
			--		result <= BUSWIDTH_ONE;
			--	else
			--		result <= BUSWIDTH_ZERO;
			--	end if;
				result(31 downto 1)<=(others=>'0');
				if (srcA(31) xor srcB(31))='1' then
					result(0)<=srcA(31);
				else
					result(0)<=subtract(32);
				end if;
			when AluControl_SLTU =>
				result(31 downto 1)<=(others=>'0');
				result(0)<=subtract(32);
			
			when AluControl_XOR =>
				result <= srcA xor srcB;

			when AluControl_SRL =>
				result  <= shifted(31 downto 0);

			when AluControl_SRA =>
				result  <=shifted(31 downto 0);

			when AluControl_OR =>
				result <= srcA or srcB;

			when AluControl_AND =>
				result <= srcA and srcB;
			when AluControl_MUL =>				
				result<=frommul.c;
			when AluControl_CLZ =>
				--leading_zeroes32(from_e.srcA,unsigned(tmp),result_v);	
				result<=x"000000"&"00"&fromb.result;--clz32(from_e.srcA);--result_v;
		end case;
	end process;
end;