library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity HazardUnit is
	generic (
    	register_bypass      : std_logic := '1'--1 is enable, 0 disable (for singlecycle operation)      
    	);
	port (
		from_bu: in Intcon_BU2HU;
		from_f: in Intcon_F2HU;
		from_d: in Intcon_D2HU;
		from_e: in Intcon_E2HU;
		from_m: in Intcon_M2HU;
		from_w: in Intcon_W2HU;		
		to_e: out Intcon_HU2E;

		hazards: out Hazards_t
	);
end;

architecture rtl of HazardUnit is
	--Cause:	branch, raised from M
	--Priority:	middling, because raised in M it deletes everything in earlier stages
	--Effect:
	--	flush F2D, because that instr must not be executed
	--	flush D2E, because that instr must not be executed
	--  flush E2M, because reaction to stall is at next clk edge, so this flushes the next instruction, not the jump one that is in M when this raises
	signal branch: boolean;

	--Cause:	in D there is an instruction that needs result of LW that is in E. This can not be forwarded.
	--Priority: lowest, because branch deletes it and mstall conflicts with it;
	--that is, when mstall, the instr will not move into E anywaybut the LW in E will be lost because M is occupied
	--Effect:
	--	stall PC2F, because D is occupied
	--	stall F2D, because D is occupied by the instr that can not move into E.
	--	flush D2E, because the instr in D can not be executed correctly yet. Because the flush occurs on next clock raise, the LW will move to M, but E will be empty.
	signal stall: boolean;

	--Cause:	waiting for M
	--Priority: independent, because it is strenghtened stall and it can not be raised alongside branch
	--Effect:
	--	stall PC2F, because M is occupied
	--	stall F2D, because M is occupied
	--	stall D2E, because M is occupied
	--	stall E2M, because M is occupied
	--	stall M2W, because result of M is not yet ready. TODO: why not flush?
	signal mstall: boolean;

	--Cause:	The wait in M is caused by external data mem, not multistage operations in M.
	--Priority: independent, because can only be raised alongside mstall to strenghten it.
	--Effect:
	--	stall M2M, the M feedback loop, so that state of multistage mem access can not advance until one word of data is fetched from memory.
	signal extmstall: boolean;

	--Cause:	Waiting to fetch instruction.
	--Priority:	Highest, when fetch is blocked, branch can not be executed.
	--Effect:	
	--	stall PC2F to keep reading the same address
	--	stall everything until E2M, because there is no register between E2M and PC2F, and we can not guarantee abort on instruction read
	--			so while fetch is stalled, the next address must be held in E2M
	--Original effect
	--	flush F2D, because valid instruction not available  
	signal istall: boolean;

	--Cause:	Multicycle ALU operation.
	--Priority:	Low, a subset of mstall that can be flushed by branch.
	--Effect:
	--	stall PC2F, because E is occupied
	--	stall F2D, because E is occupied
	--	stall D2E, because E is occupied
	--	stall E2M, because E result is not ready and E2M may also hold a jump that can not be executed now
	signal estall: boolean;
begin
	hazards.trap <= from_m.trap;
	
	hazards.stall_fetch <= '1' when (stall or mstall or istall or estall) else '0';--needs to be raised whenever execution blocked to stop PC
	hazards.stall_decode <= '1' when (stall or mstall or istall or estall) else '0';
	hazards.flush_decode <= '1' when (branch) else '0';
	hazards.flush_execute <= '1' when (branch or stall) else '0';
	hazards.flush_memory <= '1' when branch else '0';
	hazards.stall_all <= '1' when (mstall or istall or estall) else '0';
	--mem feedback is stopped when M is stalled even if mstall not raised; this allows the result of multicycle operation to be held
	hazards.stall_mem_feedback <= '1' when (extmstall or ((istall or estall) and not mstall)) else '0';
	estall <= not branch and from_e.ewait='1';
	branch <= not mstall and not istall and from_bu.branch_outcome = '1';--middle priority
	stall <= --(from_m.mem_wait/='1' and from_m.unalign/='1')and  from_bu.branch_outcome /= '1' and (--lowest priority
				not (branch or mstall or istall) --
				and(--lowest priority
		(
			from_e.mem_to_reg = '1' and from_e.rd /= REGSEL_ZERO and (
				from_d.rs1 = from_e.rd
				or from_d.rs2 = from_e.rd
			)
		) or 
		(
			register_bypass='0' and --if register bypassing is active, this stall is redundant
			(
				from_w.reg_write = '1' and from_w.rd /= REGSEL_ZERO and (
					from_d.rs1 = from_w.rd
					or from_d.rs2 = from_w.rd
				)
			)
		)
	);
	mstall<= (from_m.mem_wait='1' or from_m.unalign='1');--independent
	-- and from_bu.branch_outcome /= '1';--middle priority. 
	extmstall<= from_m.mem_wait='1';-- and from_bu.branch_outcome /= '1';--strenghtening of mstall when the delay is created in memory, not its control
	istall<= from_f.mem_wait='1';
	forwards: process (from_e, from_m, from_w)
	begin
		to_e.forwardA <= ForwardValue_None;
		to_e.forwardB <= ForwardValue_None;

		if from_w.reg_write = '1' and from_w.rd /= REGSEL_ZERO then
			if from_w.rd = from_e.rs1 then
				to_e.forwardA <= ForwardValue_Writeback;
			end if;
			if from_w.rd = from_e.rs2 then
				to_e.forwardB <= ForwardValue_Writeback;
			end if;
		end if;

		if from_m.reg_write = '1' and from_m.rd /= REGSEL_ZERO then
			if from_m.rd = from_e.rs1 then
				to_e.forwardA <= ForwardValue_Memory;
			end if;
			if from_m.rd = from_e.rs2 then
				to_e.forwardB <= ForwardValue_Memory;
			end if;
		end if;
	end process;
end;