library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use WORK.rvapo_pkg.all;

--Zynq should be able to do 32x32bit multiply in one cycle, but for now this serves to test multicycle E
entity Multiply is
	port (
        clock:in Clock_t;
        from_alu:in Intcon_ALU2MUL;
        to_alu:out Intcon_MUL2ALU
	);
end;

architecture rtl of Multiply is
    signal l1:unsigned(15 downto 0);
    signal l2:unsigned(15 downto 0);
    signal h1:unsigned(15 downto 0);
    signal h2:unsigned(15 downto 0);
    
    signal large1:unsigned(31 downto 0);
    signal large2:unsigned(31 downto 0);
    signal small:unsigned(31 downto 0);
    signal add1:unsigned(15 downto 0);
    signal add2:unsigned(31 downto 0);
    signal result:std_logic_vector(31 downto 0);

    signal pipeline:std_logic_vector(4 downto 0):=(others=>'0');
    signal finalsign:std_logic;
    signal asign:std_logic; 
    signal bsign:std_logic; 

    signal aunsigned:std_logic_vector(31 downto 0);
    signal bunsigned:std_logic_vector(31 downto 0);

    signal azeros:std_logic_vector(6 downto 0);
    signal bzeros:std_logic_vector(6 downto 0);

    signal diva:std_logic_vector(31 downto 0);
    signal divb:std_logic_vector(31 downto 0);
    signal divc:std_logic_vector(31 downto 0);
    signal dsub:std_logic_vector(31 downto 0);
    signal ddone:std_logic;

    signal mulc1:std_logic_vector(63 downto 0);
    signal mulc2:std_logic_vector(31 downto 0);

    signal div_active:std_logic;
    signal mul_active:std_logic;        

    signal phase:std_logic:='0';
    signal clz_request:std_logic_vector(31 downto 0);
    signal zeros_diff_boilerplate:std_logic_vector(6 downto 0);
    type DivState_t is (
        DivNone,
        DivCount, 
        DivShift,
        DivSub,
        DivIter,
        DivHold
	);
    signal dstate:DivState_t:=DivNone;
begin
	to_alu <= (
		result,
		pipeline(0) or ddone,
        clz_request
	);
    asign<=(from_alu.a(31) and from_alu.asigned);
    bsign<=(from_alu.b(31) and from_alu.bsigned);
    finalsign<=asign xor bsign;

    aunsigned<=from_alu.a when asign='0' else std_logic_vector(unsigned(not from_alu.a)+1);
    bunsigned<=from_alu.b when bsign='0' else std_logic_vector(unsigned(not from_alu.b)+1);

    --result<=divc when from_alu.operation=Mul_Div else mulc2;
    
    ddone<=not from_alu.fresh when dstate=DivHold else '0';

    div_active<='1' when from_alu.operation=Mul_Div or from_alu.operation=Mul_Rem or from_alu.operation=Mul_Divu or from_alu.operation=Mul_Remu else '0';
    mul_active<=not div_active;

    result_selector:process (from_alu.operation,diva,mulc2,divc)begin
        case(from_alu.operation)is
            when Mul_Div=>result<=divc;
            when Mul_Divu=>result<=divc;
            when Mul_Rem=>result<=diva;
            when Mul_Remu=>result<=diva;
            when others=>result<=mulc2;
        end case;
    end process;

    zeros_diff_boilerplate<=std_logic_vector(unsigned(bzeros)-unsigned(azeros)-1);
    clz_request<=bunsigned when from_alu.start='1' else diva;--the condition should be dstate=Div_None but this is faster and works too

	signed:process (from_alu,clock) 
    variable azeros_v:BusWidth_t;
    variable bzeros_v:BusWidth_t;
    variable zeros_diff:std_logic_vector(6 downto 0);
    variable tmp:BusWidth_t:=(others=>'0');
    variable tmp64:std_logic_vector(63 downto 0):=(others=>'0');
	begin
        --leading_zeroes32(diva,unsigned(tmp),azeros_v);	
        --tmp:=(others=>'0');
        --leading_zeroes32(bunsigned,unsigned(tmp),bzeros_v);	
        --azeros_v:=x"000000"&"00"&clz32(diva);
        --bzeros_v:=x"000000"&"00"&clz32(bunsigned);
		if(rising_edge(clock.pulse))then            

            if(from_alu.start='1')then
                if div_active='1' then
                    dstate<=DivCount;
                    phase<='0';
                end if;
                bzeros<="0"&from_alu.clz_result;--bzeros_v(6 downto 0);
                diva<=aunsigned;
                divb<=bunsigned;
                divc<=(others=>'0');
            end if;

            case(dstate)is           
                when DivCount=>    
                    azeros<="0"&from_alu.clz_result;--azeros_v(6 downto 0);           
                    dstate<=DivShift;
                when DivShift=>                          
                    zeros_diff:=std_logic_vector(unsigned(bzeros)-unsigned(azeros)-1);
                    if zeros_diff(5 downto 0)/="000000" and zeros_diff(6)='0' then--signed(zeros_diff)>0 then
                        dsub<=std_logic_vector(shift_left(unsigned(divb),to_integer(unsigned(zeros_diff)))); 
                        dstate<=DivSub;
                    else
                        dstate<=DivIter;
                    end if;
                when DivSub=>      
                    diva<=std_logic_vector(unsigned(diva)-unsigned(dsub));  
                    divc<=std_logic_vector(unsigned(divc)+shift_left(to_unsigned(1,32),to_integer(unsigned(zeros_diff))));
                    dstate<=DivCount;
                when DivIter=>
                    if unsigned(diva)<unsigned(divb) then
                    --    divc<=std_logic_vector(unsigned(divc)-1);
                        dstate<=DivHold;
                        if(finalsign='1')then
                            divc<=std_logic_vector(unsigned(not divc)+1);                    
                        end if;    
                    else
                        divc<=std_logic_vector(unsigned(divc)+1);
                        diva<=std_logic_vector(unsigned(diva)-unsigned(divb));  
                    end if;
                when DivHold=>
                    if from_alu.start='0' and from_alu.fresh='1' then
                        dstate<=DivNone;
                    end if;
                when others=>null;
            end case;

            if(from_alu.start='1') and mul_active='1' then
                    pipeline<="00100";    
            elsif from_alu.fresh='1' then
                pipeline<=(others=>'0');
            elsif(pipeline(0)='0')then--must hold done until fresh
                pipeline<=std_logic_vector(shift_right(unsigned(pipeline),1));
            end if;
            mulc1<=std_logic_vector(unsigned(aunsigned)*unsigned(bunsigned));
            
            --pipeline<=std_logic_vector(shift_right(unsigned(pipeline),1)) or (from_alu.start and not pipeline(4))&"0000";
        --    if(from_alu.start='1')then
        --        pipeline<="10000";
        --    elsif(pipeline(0)='0')then
        --        pipeline<=std_logic_vector(shift_right(unsigned(pipeline),1));
        --    end if;
        --    l1<=unsigned(aunsigned(15 downto 0));
        --    h1<=unsigned(aunsigned(31 downto 16));
        --    l2<=unsigned(bunsigned(15 downto 0));
        --    h2<=unsigned(bunsigned(31 downto 16));
        --    large1<=l1*h2;
        --    large2<=l2*h1;
        --    small<=l1*l2;
        --    add1<=large1(15 downto 0)+large2(15 downto 0);
        --    add2<=unsigned(std_logic_vector(add1)&x"0000")+small;
            if(finalsign='1')then
                if from_alu.operation=Mul_Signed then
                    mulc2<=std_logic_vector(unsigned(not mulc1(31 downto 0))+1);
                else 
                    tmp64:=std_logic_vector(unsigned(not mulc1)+1);
                    mulc2<=tmp64(63 downto 32);
                end if;    
            else
                if from_alu.operation=Mul_Signed then
                    mulc2<=mulc1(31 downto 0);--the calculation is done as unsigned and result should be a positive number, so leave it as is
                else
                    mulc2<=mulc1(63 downto 32);
                end if;    
            end if;
        end if;
	end process;
end;