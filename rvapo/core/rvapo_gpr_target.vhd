library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use WORK.rvapo_pkg.all;

entity GeneralPurposeRegister_Target is
	generic (
    	register_bypass      : std_logic := '1'--1 is enable, 0 disable (for singlecycle operation)      
    	);
	port (
		clock: in Clock_t;

		addr_1: in RegSel_t;
		read_1: out BusWidth_t;
		
		addr_2: in RegSel_t;
		read_2: out BusWidth_t;

		addr_3: in RegSel_t;
		write_enable_3: in std_logic;
		write_3: in BusWidth_t
	);
end;

architecture rtl of GeneralPurposeRegister_Target is
	type registers_array is array (0 to 31) of buswidth_t;
	shared variable registers: registers_array;

	-- TODO
--	signal debug_registers: registers_array;
begin
--	debug: process (clock)
--	begin
--		debug_registers <= registers;
--	end process;

	port_1: process (clock, addr_1)
		variable index: integer;
	begin
		index := to_integer(unsigned(addr_1));
		if index=0 then
			read_1<=(others=>'0');
		elsif addr_1=addr_3 and write_enable_3='1' and register_bypass='1' then
			read_1<=write_3;
		else	
			read_1 <= registers(index);
		end if;
	end process;

	port_2: process (clock, addr_2)
		variable index: integer;
	begin
		index := to_integer(unsigned(addr_2));
		if index=0 then
			read_2<=(others=>'0');
		elsif addr_2=addr_3 and write_enable_3='1' and register_bypass='1' then
			read_2<=write_3;
		else	
			read_2 <= registers(index);
		end if;
	end process;

	port_3: process (clock, addr_3, write_enable_3, write_3)
		variable index: integer;
	begin
		if rising_edge(clock.pulse) then
			if clock.reset = '1' then
				registers := (others => buswidth_zero);		
			elsif clock.enable = '1' then
				index := to_integer(unsigned(addr_3));
				if index /= 0 and write_enable_3 = '1' then
					registers(index) := write_3;
				end if;
			end if;
		end if;
	end process;
end;
