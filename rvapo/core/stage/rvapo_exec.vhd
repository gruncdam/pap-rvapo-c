library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use WORK.rvapo_pkg.all;

entity StageExecute is
	port (
		from_d: in Intcon_D2E;
		from_hu: in Intcon_HU2E;
		from_alu: in Intcon_ALU2E;
		from_m: in Intcon_M2E;
		from_w: in Intcon_W2E;
		to_hu: out Intcon_E2HU;
		to_alu: out Intcon_E2ALU;
		to_m: out Intcon_E2M;
		fresh: in std_logic
	);
end;

architecture rtl of StageExecute is
	signal srcA: BusWidth_t;
	signal srcB: BusWidth_t;

	signal pc_plus_imm: BusWidth_t;
	signal write_data: BusWidth_t;
begin
	to_alu <= (
		from_d.alu_control,
		srcA,
		srcB,
		from_d.multype,
		fresh
	);

	to_m <= (
		from_d.pc_plus_4,
		pc_plus_imm,
		from_alu.alu_out,
		from_alu.alu_flag,
		write_data,
		from_d.rd,
		from_d.control
	);

	to_hu <= (
		from_d.rs1,
		from_d.rs2,
		from_d.rd,
		from_d.control.mem_to_reg,
		from_alu.alu_wait
	);

	pc_plus_imm <= std_logic_vector(unsigned(from_d.pc) + unsigned(from_d.valI));

	sources: process (from_d, from_m, from_w, from_hu)
		variable srcB_beforeImm: BusWidth_t;
	begin
		case from_hu.forwardA is
			when ForwardValue_None =>
				srcA <= from_d.val1;
			when ForwardValue_Memory =>
				srcA <= from_m.result_data;
			when ForwardValue_Writeback =>
				srcA <= from_w.result;
		end case;

		case from_hu.forwardB is
			when ForwardValue_None =>
				srcB_beforeImm := from_d.val2;
			when ForwardValue_Memory =>
				srcB_beforeImm := from_m.result_data;
			when ForwardValue_Writeback =>
				srcB_beforeImm := from_w.result;
		end case;
		write_data <= srcB_beforeImm;

		if from_d.alu_source = AluSource_Reg then
			srcB <= srcB_beforeImm;
		else
			srcB <= from_d.valI;
		end if;
	end process;
end;
