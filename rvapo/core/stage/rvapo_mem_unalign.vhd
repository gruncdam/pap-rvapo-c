library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use WORK.rvapo_pkg.all;

entity StageMemoryUnalign is
	port (
		from_e: in Intcon_E2M;
		from_dm: in Intcon_DM2M;
		to_hu: out Intcon_M2HU;
		to_bu: out Intcon_M2BU;
		to_dm: out Intcon_M2DM;
		to_e: out Intcon_M2E;
		to_w: out Intcon_M2W;
		to_m: out Intcon_M2M;
		from_m: in Intcon_M2M;
		fresh: in std_logic;
		mem_req: out std_logic
	);
end;
architecture rtl of StageMemoryUnalign is
	signal result_data: BusWidth_t;
	signal memadr: BusWidth_t:= (others => '0');
	signal memwrt: BusWidth_t:= (others => '0');
	signal memred: BusWidth_t:= (others => '0');		
	signal deladr: BusWidth_t:= (others => '0');		
	signal memred_combined: BusWidth_t:= (others => '0');	
	signal unalign: std_logic	:='0';
	signal unalign_single: std_logic	:='0';
	signal step: std_logic_vector(1 downto 0):=(others => '0');		
	signal mem_active:std_logic;

	signal memctrl_ext:MemoryControl_t:=MemoryControl_Word;
	signal bitreg: std_logic_vector(7 downto 0):=(others => '0');
	signal next_bitreg: std_logic_vector(7 downto 0):=(others => '0');
	signal no_delay:std_logic:='0';--this is thoughtlessly copied from previous version

	function rot_4B_wrt(val: BusWidth_t;do:std_logic) return BusWidth_t is 
		variable result:BusWidth_t;
	begin
		if do='1' then result:=--val(7 downto 0)&val(31 downto 24)&val(23 downto 16)&val(15 downto 8);
								val(23 downto 16)&val(15 downto 8)&val(7 downto 0)&val(31 downto 24);
		else result:=val; end if;
		return result;
	end function;

	function rot_4B_red(val: BusWidth_t;do:std_logic) return BusWidth_t is 
		variable result:BusWidth_t;
	begin
		if do='1' then result:=val(7 downto 0)&val(31 downto 24)&val(23 downto 16)&val(15 downto 8);
		else result:=val; end if;
		return result;
	end function;	

	function rot_2B(val: BusWidth_t;do:std_logic) return BusWidth_t is 
		variable result:BusWidth_t;
	begin
		if do='1' then result:=val(15 downto 0)&val(31 downto 16);
		else result:=val; end if;
		return result;
	end function;

	function bit2byte(val:std_logic) return std_logic_vector is begin
		return val&val&val&val&val&val&val&val;
	end function;

	function bit2word(val:std_logic) return std_logic_vector is begin
		return bit2byte(val)&bit2byte(val)&bit2byte(val)&bit2byte(val);
	end function;
begin
	to_hu <= (
		from_e.rd,
		from_e.control.reg_write,
		from_e.control.trap,
		from_dm.mem_wait,
		unalign or unalign_single
	);

	to_bu <= (
		from_e.alu_out,
		from_e.alu_flag,
		from_e.pc_plus_imm,
		from_e.control.branch
	);

	to_dm <= (
		memadr,
		memwrt,--reverse_endian(memwrt),
		from_e.control.mem_write,
		bitreg(3 downto 0)--reverse_vector(bitreg(3 downto 0))--reverse_vector(mem_bytes)
	);

	to_e <= (
		result_data => result_data
	);

	to_w <= (
		extender(memctrl_ext,memred_combined),
		result_data,
		from_e.rd,
		from_e.control.reg_write,
		from_e.control.mem_to_reg,
	--	(from_dm.delayed_by_clk and from_e.control.mem_to_reg) I do not know why this was here, but it breaks stalling (see comments in intcon_pipeline)
		(from_dm.delayed_by_clk and not from_m.no_delay)--this prevents skipping the result of unaligned read (when delayed, normal read is forwarded)
	);												--also has to be 0 when ending unalign (2300ns in im_stalls)

	to_m<=(
		deladr,
		memred and bit2word(unalign),--gate so that it does not interfere with next read encase it is aligned (or is used to combine)	
		step,
		from_e.control.mem_control,
		no_delay,
		--memwrt,--next_memwrt,
		next_bitreg
		);

	bitreg_manager:process(from_e,step,from_m,bitreg,fresh) begin
		if from_m.step="00" then--refresh
			bitreg<=std_logic_vector(shift_left(unsigned(x"0"&reverse_vector(memctrl2mask(from_e.control.mem_control))),to_integer(unsigned(from_e.alu_out(1 downto 0)))));
			--	and bit2byte(fresh);
		else
			--bitreg<=std_logic_vector(shift_right(unsigned(bitreg),4));
			bitreg<=from_m.bitreg;--inherit; thus is bitreg held on unalign termination	
		end if;

		case step is--this is the step sent to next clock, so send a bitreg along
			when "00"=>
				next_bitreg<=(others=>'0');
				no_delay<='0';
			when "10"=>
				next_bitreg<=bitreg;
				no_delay<='1';
			when "01"=>
				next_bitreg<=bitreg;
				no_delay<='0';
			when "11"=>
				no_delay<='1';
			--	if unalign_single='1' then
			--		next_bitreg<=bitreg;
			--	else
				next_bitreg<=std_logic_vector(shift_right(unsigned(bitreg),4));		
			--	end if;	
			when others=>
				next_bitreg<=(others=>'1');--thus invalid value of step causes infinite loop :)
				no_delay<='0';	
		end case;
	end process;

	stepper:process(from_m,from_dm,from_e,unalign,unalign_single) begin
		if unalign_single='1' then
			step<="10";
		elsif unalign='1' then														--otherwise there is no escape
			if from_dm.delayed_by_clk='1' and from_e.control.mem_to_reg='1' and from_m.step="00" then--three step read
				step<="01";	
			else--two step
				step<="11";	--go straight to final
			end if;	
		else
			step<="00";--step is always 00 on termination, this way when pipeline moves, from_m.step will be too
		end if;

		if from_dm.delayed_by_clk='1' then memctrl_ext<=from_m.mem_control;--this is thoughtlessly copied from previous version
		else memctrl_ext<=from_e.control.mem_control; end if;
	end process;
	mem_req<=fresh;-- or unalign or unalign_single;
	mem_active<=from_e.control.mem_to_reg or from_e.control.mem_write;
	--unalign, whether two or three steps, terminates by upper bitreg shifting out and holds by stalling M feedback, thereby maintaining bitreg, step and deladr
	--unalign_single terminates by step and hold by step="10", a special value that holds the initial address as if 00 
	--align does not change state
	unalign<=(bitreg(4) or bitreg(5) or bitreg(6) or bitreg(7)) and mem_active;
	unalign_single<=(from_e.alu_out(1) or from_e.alu_out(0)) and ((not from_m.step(1)) and (not from_m.step(0))) and (not unalign) and from_e.control.mem_to_reg and from_dm.delayed_by_clk; 
	--step<=unalign&unalign;
	deladr<=std_logic_vector(to_unsigned((to_integer(unsigned(from_e.alu_out(31 downto 2))))+1,memadr'length-2))&"00";--this will always contain lowalign(addr)+4
	memadr<=(from_e.alu_out(31 downto 2)&"00" and bit2word(mem_active)) when from_m.step="00" or from_m.step="10"
		 else from_m.addrdelayer;--gate addr when no mem to avoid unnecessary AXI op
	memred<=rot_4B_red(rot_2B(from_dm.read_data and bit2byte(bitreg(3))&bit2byte(bitreg(2))&bit2byte(bitreg(1))&bit2byte(bitreg(0)),from_e.alu_out(1)),from_e.alu_out(0));-- when unalign='1' else (others=>'0');
	memwrt<=rot_4B_wrt(rot_2B(from_e.write_data,from_e.alu_out(1)),from_e.alu_out(0));
	--memwrt<=from_e.write_data;

	memred_combined<=memred or from_m.datadelayer;

	process (from_e, from_dm)
	begin
		if from_e.control.branch = BCtrl_JAL or from_e.control.branch = BCtrl_JALR then
			result_data <= from_e.pc_plus_4;
		elsif from_e.control.pc_plus_imm_to_reg = '1' then
			result_data <= from_e.pc_plus_imm;
		else 
			result_data <= from_e.alu_out;
		end if;
	end process;
end;
