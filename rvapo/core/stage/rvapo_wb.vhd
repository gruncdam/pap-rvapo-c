library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity StageWriteback is
	port (
		from_m: in Intcon_M2W;
		to_hu: out Intcon_W2HU;
		to_gpr: out Intcon_W2GPR;
		to_e: out Intcon_W2E;
		fresh: in std_logic;
		clock: in Clock_t
	);
end;

architecture rtl of StageWriteback is
	signal result: BusWidth_t;
	--this is necessary because when normal read with reg bypass is followed by a long, non-bypassed operation
	--then the bypassing value is avaliable just for the first clock cycle of the long operation
	--after which the memory gives data for the long operation
	--so a register has to be inserted to hold the bypassed value for forwarding
	--in regwrite this is solved by gating write enable by fresh
	signal read_reg: BusWidth_t;
begin
	to_hu <= (
		from_m.rd,
		from_m.reg_write
	);
	to_gpr <= (
		result,
		from_m.rd,
		from_m.reg_write and fresh
	);
	to_e <= (
		result => result
	);

	process (from_m,clock)
	begin
		if from_m.mem_to_reg = '1' then
			if fresh='1' then
				result <= from_m.read_data;
			else
				result <= read_reg;
			end if;
		else
			result <= from_m.result_data;
		end if;

		if rising_edge(clock.pulse) then
			if fresh='1' then
				read_reg<=from_m.read_data;
			end if;
		end if;
	end process;
end;