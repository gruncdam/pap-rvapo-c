library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use WORK.rvapo_pkg.all;
library std;
use STD.textio.all;
use ieee.std_logic_textio.all;

use WORK.rvapo_cm_target_init.all;

entity CombinedMemory_Target is
	generic (
		dump_mem:	boolean;
		output_filename:string;
		delayed      	:std_logic
		);
	port (
		clock: in Clock_t;

		addr_1: in BusWidth_t;
		read_1: out BusWidth_t;
		write_1: in BusWidth_t;
		web: in std_logic_vector(3 downto 0);

		addr_2: in BusWidth_t;
		read_2: out BusWidth_t
	);
end;

architecture rtl of CombinedMemory_Target is
	shared variable memory: memory_array_t := MEMORY_INIT;
	file mem_dump : text;
	subtype Index_t is unsigned(BusWidth_t'range);
	constant MEMORY_HIGH_INDEX: Index_t := to_unsigned(memory'high - 4, BusWidth_t'length);
	signal oldreset:std_logic:='0';
begin
	on_reset: process (clock)
	variable OLINE     : line;
	variable a: String(1 to 1) := ":";
	begin
		if rising_edge(clock.pulse) then
			oldreset<=clock.reset;
		end if;

		if oldreset='0' and clock.reset='1' and rising_edge(clock.pulse) then
			file_open(mem_dump, output_filename, write_mode);
			for i in 0 to 2000 loop 
				hwrite(OLINE, std_logic_vector(to_unsigned(i*4,16)), right, 4);
				write(OLINE, a, right, 4);
				hwrite(OLINE, memory(i*4+3),right,3);
				hwrite(OLINE, memory(i*4+2),right,3);
				hwrite(OLINE, memory(i*4+1),right,4);
				hwrite(OLINE, memory(i*4),right,3);
				writeline(mem_dump,OLINE);
			end loop;
			file_close(mem_dump);

			memory := MEMORY_INIT;
		end if;
	end process;

	port_1: process (clock, addr_1)
		variable index: Index_t;
		variable result: BusWidth_t;
	begin
		if clock.reset = '1' then
			read_1 <= BUSWIDTH_ZERO;
		elsif clock.enable = '1' and (rising_edge(clock.pulse) or delayed='0') then
			index := unsigned(addr_1);

			if index <= MEMORY_HIGH_INDEX then
				result := memory(to_integer(index) + 3) & memory(to_integer(index) + 2) & memory(to_integer(index) + 1) & memory(to_integer(index));
			else
				result := BUSWIDTH_ZERO;
			end if;

			read_1 <= result;
		end if;
	end process;

	port_1_write: process (clock, addr_1, write_1)
		variable index: Index_t;
	begin
		if clock.reset /= '1' and clock.enable = '1' and rising_edge(clock.pulse) then
			index := unsigned(addr_1);

			if index <= MEMORY_HIGH_INDEX then
				if web(0)='1' then memory(to_integer(index)):=write_1(7 downto 0);end if;
				if web(1)='1' then memory(to_integer(index)+1):=write_1(15 downto 8);end if;
				if web(2)='1' then memory(to_integer(index)+2):=write_1(23 downto 16);end if;
				if web(3)='1' then memory(to_integer(index)+3):=write_1(31 downto 24);end if;
			end if;
		end if;
	end process;

	port_2: process (clock, addr_2)
		variable index: Index_t;
		variable result: BusWidth_t;
	begin
		if clock.reset = '1' then
			read_2 <= BUSWIDTH_ZERO;
		elsif clock.enable = '1' and (rising_edge(clock.pulse) or delayed='0') then
			index := unsigned(addr_2);

			if index <= MEMORY_HIGH_INDEX then
				result := memory(to_integer(index) + 3) & memory(to_integer(index) + 2) & memory(to_integer(index) + 1) & memory(to_integer(index));
			else
				result := BUSWIDTH_ZERO;
			end if;

			read_2 <= result;
		end if;
	end process;
end;
