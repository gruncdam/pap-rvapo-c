library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
use WORK.rvapo_pkg.all;
library std;
use std.textio.all;

package rvapo_cm_target_init is
    subtype Byte_t is std_logic_vector(7 downto 0);
    type memory_array_t is array (0 to 65535 + 4) of Byte_t;

    impure function init_mem_from_file (file_name : string) return memory_array_t;

    shared variable MEMORY_INIT: memory_array_t := init_mem_from_file("../software/out/hexstr/rvapo_mem_content.txt"); 
end package rvapo_cm_target_init;

package body rvapo_cm_target_init is
	impure function init_mem_from_file (file_name : string) return memory_array_t is
		file table_file    : text open read_mode is file_name;
		variable file_line : line;
		variable result    : memory_array_t;
		variable v_data_read  : integer;
	begin
		for i in memory_array_t'range loop
			loop
				if (endfile(table_file)) and (i /= 0) then
					-- Repeat file data when not enough data to fill
					file_close(table_file);
					file_open(table_file, file_name, read_mode);
				end if;
				readline(table_file, file_line);
				exit when file_line'length /= 0;
			end loop;

			read(file_line, v_data_read);
			result(i):=std_logic_vector(to_unsigned(v_data_read,8));
		end loop;
		return result;
	end function init_mem_from_file;
end;
