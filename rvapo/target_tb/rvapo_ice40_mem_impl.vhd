library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use WORK.rvapo_pkg.all;

--use WORK.rvapo_ice40_mem_init.all;

entity SB_RAM40_4K is
	generic(
		INIT_0:std_logic_vector(255 downto 0);--reverse!
		INIT_1:std_logic_vector(255 downto 0);
		INIT_2:std_logic_vector(255 downto 0);
		INIT_3:std_logic_vector(255 downto 0);
		INIT_4:std_logic_vector(255 downto 0);
		INIT_5:std_logic_vector(255 downto 0);
		INIT_6:std_logic_vector(255 downto 0);
		INIT_7:std_logic_vector(255 downto 0);
		INIT_8:std_logic_vector(255 downto 0);
		INIT_9:std_logic_vector(255 downto 0);
		INIT_A:std_logic_vector(255 downto 0);
		INIT_B:std_logic_vector(255 downto 0);
		INIT_C:std_logic_vector(255 downto 0);
		INIT_D:std_logic_vector(255 downto 0);
		INIT_E:std_logic_vector(255 downto 0);
		INIT_F:std_logic_vector(255 downto 0);
		WRITE_MODE:integer;
  		READ_MODE:integer
		);
	 port(
		RDATA:	out std_logic_vector(15 downto 0);
		RADDR:	in std_logic_vector(10 downto 0);
		WADDR:	in std_logic_vector(10 downto 0);
		MASK:	in std_logic_vector(15 downto 0);
		WDATA:	in std_logic_vector(15 downto 0);
		RCLKE: 	in std_logic;	
		RCLK: 	in std_logic;
		RE: 	in std_logic;		
		WCLKE: 	in std_logic;		
		WCLK: 	in std_logic;
		WE: 	in std_logic
		);
end;

architecture rtl of SB_RAM40_4K is
	subtype Line_t is std_logic_vector(255 downto 0);
	type memory_array_t is array (0 to 15) of Line_t;
	shared variable memory: memory_array_t:=(0=>INIT_0,1=>INIT_1,2=>INIT_2,3=>INIT_3,4=>INIT_4,5=>INIT_5,6=>INIT_6,7=>INIT_7,8=>INIT_8,9=>INIT_9,10=>INIT_A,11=>INIT_B,12=>INIT_C,13=>INIT_D,14=>INIT_E,15=>INIT_F);

	constant MEMORY_HIGH_INDEX:integer:=255;
	signal rtest:std_logic_vector(15 downto 0);
begin
	read:process(RCLK) 
	variable index: integer;
	begin
		if rising_edge(RCLK) then
			if RCLKE='1' and RE='1' then --and index<=MEMORY_HIGH_INDEX then
				RDATA <=memory(to_integer(unsigned(RADDR(7 downto 4))))(
														((to_integer(unsigned(RADDR(3 downto 0)))+1)*16)-1 
														downto 
														(to_integer(unsigned(RADDR(3 downto 0)))*16)
														);						
			else
				RDATA<=(others=>'0');
			end if;
		end if;
	end process;
WRITER_GENERATE: for i in 0 to 15 generate	
	write:process(WCLK) 
	variable index: integer;
	begin
		if rising_edge(WCLK) then
			if WCLKE='1' and WE='1' then --and index<=MEMORY_HIGH_INDEX then
				if MASK(i)='0' then memory(to_integer(unsigned(RADDR(7 downto 4))))
																	(
																	(to_integer(unsigned(RADDR(3 downto 0)))*16)+i
																	):=WDATA(i); 
				end if;			
			end if;
		end if;
	end process;
end generate WRITER_GENERATE;	
end;
