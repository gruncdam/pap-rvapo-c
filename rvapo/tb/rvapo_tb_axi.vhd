library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
use WORK.rvapo_pkg.all;
use WORK.rvapo_cm_target_init.all;--thus is fileloader imported

entity rvapo_tb_axi is
end entity;

architecture tb of rvapo_tb_axi is
	signal clock: clock_t := (
		pulse => '1',
		enable => '0',
		reset => '1'
	);

	signal trapped: std_logic_vector(5 downto 0);
	signal done: std_logic := '0';

	signal M_AXI_ACLK          :    std_logic;
	
	signal M_AXI_AWID          :    std_logic_vector(5 DOWNTO 0);
    signal M_AXI_AWADDR        :    std_logic_vector(31 downto 0);
    signal M_AXI_AWLEN         :    std_logic_vector(3 downto 0);
    signal M_AXI_AWSIZE        :    std_logic_vector(2 downto 0);
    signal M_AXI_AWBURST       :    std_logic_vector(1 downto 0);
    signal M_AXI_AWLOCK        :    std_logic_vector(1 downto 0);
    signal M_AXI_AWCACHE       :    std_logic_vector(3 downto 0);
    signal M_AXI_AWPROT        :    std_logic_vector(2 downto 0);
    signal M_AXI_AWQOS         :    std_logic_vector(3 downto 0);
    signal M_AXI_AWUSER        :    std_logic_vector(4 downto 0);
    signal M_AXI_AWVALID       :    std_logic;
	signal M_AXI_AWREADY	   :    std_logic;		
	
	signal M_AXI_WID           :    std_logic_vector(5 downto 0);
    signal M_AXI_WDATA         :    std_logic_vector(31 downto 0);
    signal M_AXI_WSTRB         :    std_logic_vector(3 downto 0);
    signal M_AXI_WLAST         :    std_logic;
    signal M_AXI_WVALID        :    std_logic;
	signal M_AXI_WREADY		   :    std_logic;
		
	signal M_AXI_BRESP		   :	std_logic_vector(1 downto 0);
	signal M_AXI_BVALID		   :    std_logic;
	signal M_AXI_BREADY        :    std_logic;
	
	signal M_AXI_ARID          :    std_logic_vector(5 downto 0);
    signal M_AXI_ARADDR        :    std_logic_vector(31 downto 0);
    signal M_AXI_ARLEN         :    std_logic_vector(3 downto 0);
    signal M_AXI_ARSIZE        :    std_logic_vector(2 downto 0);
    signal M_AXI_ARBURST       :    std_logic_vector(1 downto 0);
    signal M_AXI_ARLOCK        :    std_logic_vector(1 downto 0);
    signal M_AXI_ARCACHE       :    std_logic_vector(3 downto 0);
    signal M_AXI_ARPROT        :    std_logic_vector(2 downto 0);
    signal M_AXI_ARQOS         :    std_logic_vector(3 downto 0);
    signal M_AXI_ARUSER        :    std_logic_vector(4 downto 0);
    signal M_AXI_ARVALID       :    std_logic;
	signal M_AXI_ARREADY	   :	std_logic;
	
	signal M_AXI_RDATA         :    std_logic_vector(31 downto 0);--(2**axi_data_width_log2b - 1 downto 0);
	signal M_AXI_RRESP         :    std_logic_vector(1 downto 0);
	signal M_AXI_RVALID        :    std_logic;
	signal M_AXI_RREADY		   :	std_logic;		
begin
	eut: WrapperAXIMemd port map (
		clk=>clock.pulse, 
		rst=>clock.reset,
		trapped=>trapped,

		M_AXI_AWID			=>M_AXI_AWID,
		M_AXI_AWADDR        => M_AXI_AWADDR,
        M_AXI_AWLEN         => M_AXI_AWLEN,
        M_AXI_AWSIZE        => M_AXI_AWSIZE,
        M_AXI_AWBURST       => M_AXI_AWBURST,
        M_AXI_AWCACHE       => M_AXI_AWCACHE,
        M_AXI_AWUSER        => M_AXI_AWUSER,
        M_AXI_AWVALID       => M_AXI_AWVALID,
        M_AXI_AWREADY       => '1',
        
        M_AXI_WDATA         => M_AXI_WDATA,
        M_AXI_WSTRB         => M_AXI_WSTRB,
        M_AXI_WLAST         => M_AXI_WLAST,
        M_AXI_WVALID        => M_AXI_WVALID,
        M_AXI_WREADY        => '1',
        
        M_AXI_BID          	=> "000000",
        M_AXI_BRESP         => "00", --response to write: OK
        M_AXI_BVALID        => '1',
        M_AXI_BREADY        => M_AXI_BREADY,
        
        M_AXI_ARID				=>M_AXI_ARID,
        M_AXI_ARADDR            => M_AXI_ARADDR,
        M_AXI_ARLEN             => M_AXI_ARLEN,
        M_AXI_ARSIZE            => M_AXI_ARSIZE,
        M_AXI_ARBURST           => M_AXI_ARBURST,
        M_AXI_ARCACHE           => M_AXI_ARCACHE,
        M_AXI_ARUSER            => M_AXI_ARUSER,
        M_AXI_ARVALID           => M_AXI_ARVALID,   
        M_AXI_ARREADY           => '1',--mock, always read to accept address
        
        M_AXI_RID				=>"000000",
        M_AXI_RDATA             => x"ABCDDCBA",
        M_AXI_RRESP             => "00",
        M_AXI_RLAST             => '1',
        M_AXI_RVALID            => '1',
        M_AXI_RREADY            => M_AXI_RREADY
		);

	clock.reset <= '0' after 5 ns;
	clock.enable <= '1' after 10 ns;
	clock.pulse <= not clock.pulse after 10 ns when done /= '1' else '0';

	process
	begin
		-- reset
		wait for 10 ns;
		
		-- running code
		wait until trapped /= "000000";
		wait for 10 ns;

		-- turn off the clock
		done <= '1';
		assert false report "Done, check wave output for result" severity note;
		wait;
	end process;
end architecture;