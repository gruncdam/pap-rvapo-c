library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity rvapo_tb_bu is
end entity;

architecture tb of rvapo_tb_bu is
	type test_vector is record
		from_m: Intcon_M2BU;
		to_hu: Intcon_BU2HU;
		to_pc: Intcon_BU2PC;
	end record;

	type test_vector_array is array (natural range <>) of test_vector;
	constant test_vectors: test_vector_array := (
		(
			from_m => (
				alu_out => x"00000000", alu_flag => (zero => '1'),
				pc_plus_imm => x"11111111", branch => BranchControl_None
			),
			to_hu => (branch_outcome => '0'),
			to_pc => (pc_override => '0', pc_value => x"11111111")
		),
		(
			from_m => (
				alu_out => x"00000000", alu_flag => (zero => '1'),
				pc_plus_imm => x"11111111", branch => BranchControl_BEQ
			),
			to_hu => (branch_outcome => '1'),
			to_pc => (pc_override => '1', pc_value => x"11111111")
		),
		(
			from_m => (
				alu_out => x"22222222", alu_flag => (zero => '0'),
				pc_plus_imm => x"11111111", branch => BranchControl_BEQ
			),
			to_hu => (branch_outcome => '0'),
			to_pc => (pc_override => '0', pc_value => x"11111111")
		),
		(
			from_m => (
				alu_out => x"22222222", alu_flag => (zero => '0'),
				pc_plus_imm => x"11111111", branch => BranchControl_JALR
			),
			to_hu => (branch_outcome => '1'),
			to_pc => (pc_override => '1', pc_value => x"22222222")
		),
		(
			from_m => (
				alu_out => x"00000000", alu_flag => (zero => '1'),
				pc_plus_imm => x"11111111", branch => BranchControl_BGEU
			),
			to_hu => (branch_outcome => '1'),
			to_pc => (pc_override => '1', pc_value => x"11111111")
		)
	);

	signal actual_test: test_vector;
	signal expected_test: test_vector;
begin
	eut: BranchUnit port map (
		actual_test.from_m,
		actual_test.to_hu,
		actual_test.to_pc
	);

	process
		variable expected_test_var: test_vector;
	begin
		for i in test_vectors'range loop
			expected_test_var := test_vectors(i);
			expected_test <= expected_test_var;

			actual_test.from_m <= expected_test_var.from_m;
			wait for 1 ns;

			assert actual_test.to_hu = expected_test_var.to_hu
			report "invalid to_hu" severity failure;

			assert actual_test.to_pc = expected_test_var.to_pc
			report "invalid to_pc" severity failure;
		end loop;

		assert false report "Branch unit tests passed" severity note;
		wait;
	end process;
end architecture;