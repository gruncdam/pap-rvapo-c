library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity rvapo_tb_pc is
end entity;

architecture tb of rvapo_tb_pc is
	type test_vector is record
		from_f: Intcon_F2PC;
		from_bu: Intcon_BU2PC;
		to_f: Intcon_PC2F;
	end record;

	type test_vector_array is array (natural range <>) of test_vector;
	constant test_vectors: test_vector_array := (
		(
			(pc_plus_4 => x"AABBCCDD"),
			(pc_override => '0', pc_value => x"11223344"),
			(PC => x"AABBCCDD")
		),
		(
			(pc_plus_4 => x"AABBCCDD"),
			(pc_override => '1', pc_value => x"11223344"),
			(PC => x"11223344")
		)
	);

	signal actual_test: test_vector;
	signal expected_test: test_vector;
begin
	eut: StageProgramCounter port map (actual_test.from_f, actual_test.from_bu, actual_test.to_f);

	process
		variable expected_test_var: test_vector;
	begin
		for i in test_vectors'range loop
			expected_test_var := test_vectors(i);
			expected_test <= expected_test_var;

			actual_test.from_f <= expected_test_var.from_f;
			actual_test.from_bu <= expected_test_var.from_bu;
			wait for 1 ns;

			assert actual_test.to_f = expected_test_var.to_f
			report "invalid to_f" severity failure;
		end loop;

		assert false report "PC tests passed" severity note;
		wait;
	end process;
end architecture;