library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

entity rvapo_tb_wb is
end entity;

architecture tb of rvapo_tb_wb is
	type test_vector is record
		from_m: Intcon_M2W;
		to_hu: Intcon_W2HU;
		to_gpr: Intcon_W2GPR;
		to_e: Intcon_W2E;
	end record;

	type test_vector_array is array (natural range <>) of test_vector;
	constant test_vectors: test_vector_array := (
		(
			from_m => (x"11111111", x"22222222", "00001", reg_write => '0', mem_to_reg => '0'),
			to_hu => ("00001", reg_write => '0'),
			to_gpr => (x"22222222", "00001", reg_write => '0'),
			to_e => (result => x"22222222")
		),
		(
			from_m => (x"11111111", x"22222222", "10000", reg_write => '0', mem_to_reg => '1'),
			to_hu => ("10000", reg_write => '0'),
			to_gpr => (x"11111111", "10000", reg_write => '0'),
			to_e => (result => x"11111111")
		),
		(
			from_m => (x"11111111", x"22222222", "10101", reg_write => '1', mem_to_reg => '1'),
			to_hu => ("10101", reg_write => '1'),
			to_gpr => (x"11111111", "10101", reg_write => '1'),
			to_e => (result => x"11111111")
		),
		(
			from_m => (x"11111111", x"22222222", "00000", reg_write => '1', mem_to_reg => '0'),
			to_hu => ("00000", reg_write => '1'),
			to_gpr => (x"22222222", "00000", reg_write => '1'),
			to_e => (result => x"22222222")
		)
	);

	signal actual_test: test_vector;
	signal expected_test: test_vector;
begin
	eut: StageWriteback port map (actual_test.from_m, actual_test.to_hu, actual_test.to_gpr, actual_test.to_e);

	process
		variable expected_test_var: test_vector;
	begin
		for i in test_vectors'range loop
			expected_test_var := test_vectors(i);
			expected_test <= expected_test_var;

			actual_test.from_m <= expected_test_var.from_m;
			wait for 1 ns;

			assert actual_test.to_hu = expected_test_var.to_hu
			report "invalid to_hu" severity failure;

			assert actual_test.to_gpr = expected_test_var.to_gpr
			report "invalid to_gpr" severity failure;

			assert actual_test.to_e = expected_test_var.to_e
			report "invalid to_e" severity failure;
		end loop;

		assert false report "Writeback tests passed" severity note;
		wait;
	end process;
end architecture;