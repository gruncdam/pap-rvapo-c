#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
/**
This program checks memory dump of rvapo against provided file.
*/
#define REENCODE_TMP    "./comparer_tmp.txt"
#define DEFAULT_CHECK   "rvapo_mem_dump.txt"
#define MAXLINELEN      50
#define LBYTE(a)        (unsigned char)(a&0xFF)           
#define HELP "ARGV\n \
[0] reserved\n \
[1] ref:    file to compare against, mandatory\n \
[2] input:  file to check, defaults to rvapo_mem_dump.txt\n "
FILE*reencode(FILE*trg,int addroffset){
    FILE*ret=fopen(REENCODE_TMP,"w+");

    unsigned int trgpc;//,trgb0,trgb1,trgb2,trgb3;
    unsigned int trgdata[4];
    while(fscanf(trg," %x : %x %x %x %x ",&trgpc,&trgdata[0],&trgdata[1],&trgdata[2],&trgdata[3])==5)
        for(int i=0;i<4;i++)fprintf(ret,"%04x   : %02hhx %02hhx  %02hhx %02hhx\n",trgpc+(i*4)-addroffset,
            LBYTE(trgdata[i]>>24),LBYTE(trgdata[i]>>16),LBYTE(trgdata[i]>>8),LBYTE(trgdata[i]));
    fclose(trg);
    fseek(ret,0,SEEK_SET);
    return ret;
    }
/**
 **/
int main(int argc, char *argv[]){
    if(argc<2){
        printf("At least one argument is required.\n");
        printf(HELP);
        return 101;
        }
    FILE*ref=fopen(argv[1],"r");//*/
   // FILE*ref=fopen("../software/c/testresults/mem_unalign_large.txt","r");
    if(ref==NULL){
        printf("Reference file %s not found.\n",argv[1]);
        return 101;
        }
    FILE*trg=argc==3?fopen(argv[2],"r"):fopen(DEFAULT_CHECK,"r");
    if(trg==NULL){
        fclose(ref);
        printf("File to check not found.\n");
        return 102;
        }
    unsigned int startaddr,len;
    fscanf(trg,"%x%n ",&startaddr,&len);
    fseek(trg,0,SEEK_SET);
    int reencoded=0;
    if(len>4){
        reencoded=1;
        trg=reencode(trg,startaddr);
        if(trg==NULL){
            fclose(ref);
            printf("Reencode of rdwrmem format failed.\n");
            return 103;
            }
        }

    unsigned int refpc;
    char refb0,refb1,refb2,refb3;
    char trgline[MAXLINELEN];
    int correct=0,total=0;
    while(fscanf(ref," %x : %hhx %hhx %hhx %hhx ",&refpc,&refb3,&refb2,&refb1,&refb0)==5){
        total++;
        unsigned int trgpc;
        int mov;
        int fail=0;
        do{
            if(feof(trg)!=0){
                fail=1;
                break;
                }
            fgets(trgline,MAXLINELEN,trg);
            trgline[MAXLINELEN-1]=0;            
            if(sscanf(trgline," %x : %n",&trgpc,&mov)==0){
                printf("Invalid line pc %s.\n",trgline);
                continue;
                }
            }while(trgpc!=refpc);
        if(fail==1){
            printf("Could not find a record with address 0x%x in memory dump.\n",refpc);
            break;
            }
        char*rest=trgline+mov;
        char trgb0,trgb1,trgb2,trgb3;
        if(sscanf(rest," %hhx %hhx %hhx %hhx ",&trgb3,&trgb2,&trgb1,&trgb0)!=4){
            printf("Invalid line val %s in %s.\n",rest,trgline);
            continue;
            }
        if(trgb0!=refb0||trgb1!=refb1||trgb2!=refb2||trgb3!=refb3)
            printf("ADDR %04x: %02hhx %02hhx  %02hhx %02hhx should be %02hhx %02hhx  %02hhx %02hhx\n",refpc,trgb3,trgb2,trgb1,trgb0, refb3,refb2,refb1,refb0);
        else correct++;
        }
    printf("%d/%d correct.\n",correct,total);
    if(reencoded)remove(REENCODE_TMP);
    fclose(ref);
    fclose(trg);
    return 0;
    }