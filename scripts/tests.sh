#!/bin/bash

runtest(){
    echo "Test $1"
    (cd ../software/c/ && make "CFILE=tests/$1.c") > /dev/null
    sed -i -e "509,512d" ../software/out/hexstr/rvapo_mem_content.txt
    sed -i -e "509i 00\n00\n01\n00" ../software/out/hexstr/rvapo_mem_content.txt
    ../rvapo/build.sh r > /dev/null #because lx_fncapprox's use of dont care clogs terminal otherwise
    ./comparer.out "../software/c/testresults/$1.txt" 
    }
datatest(){
    echo "DTest $1"
    (cd ../software/c/ && make "CFILE=prog/$1.c") > /dev/null
    lines=$(wc -l<"$2");    #count lines of data file
    lines=$((lines+11));
    sed -i -e "12,${lines}d" ../software/out/hexstr/rvapo_mem_content.txt   #delete lines from memory init file produced by buildc
    sed -i -e "12r $2" ../software/out/hexstr/rvapo_mem_content.txt #insert data file
    ../rvapo/build.sh r > /dev/null #because lx_fncapprox's use of dont care clogs terminal otherwise
    ./comparer.out "../software/c/testresults/$1.txt" 
    }

ln -s ../integration/vivado/ip/lx-fncapprox/reci_tab_a.dat  reci_tab_a.dat
ln -s ../integration/vivado/ip/lx-fncapprox/reci_tab_bc.dat reci_tab_bc.dat
ln -s ../integration/vivado/ip/lx-fncapprox/sin_tab_a.dat   sin_tab_a.dat
ln -s ../integration/vivado/ip/lx-fncapprox/sin_tab_bc.dat  sin_tab_bc.dat

../rvapo/build.sh f
gcc comparer.c -o comparer.out

runtest mem_unalign_large
runtest exe_stalls
runtest jump_hazards
runtest im_stalls
runtest mem_unalign_stalls
runtest mem_unalign_axi
runtest clz
runtest M
runtest fnc
runtest alu_smoke
datatest qsort tosort.txt

echo "Test interrupts."
echo "All registers should be preserved after interrupt, but the user code that test if interrupt arrived rewrites some."
echo "Therefore, 0x3E8 and 0x1000 are not actually errors."

(cd ../software/c/interrupt && make "CFILE=tests/irq.c irq_handler.S") > /dev/null
../rvapo/build.sh r > /dev/null #because lx_fncapprox's use of dont care clogs terminal otherwise
./comparer.out "../software/c/testresults/irq.txt" 

