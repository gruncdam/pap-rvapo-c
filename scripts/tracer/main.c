#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>
#include "mzapo_phys.h"
#include "pxmcc_types.h"
#define FAIL(a,b)           do{printf(a);return b;}while(0);
#define PARAMERR            "Mode (-i or -w), pxmcc_data_t address and pmsm address required.\n"
#define SW(addr)		    (*(volatile uint32_t*)(addr))	
#define LW(addr)		    *((volatile uint32_t*)(addr))
#define MIN(a,b)            (((a)>(b))?(b):(a))
#define PWM_LIM             100
#define iINTERVAL            2000
#define wINTERVAL            2000
//#define FP_CONVERT(a)   (((((uint32_t)a)>>16)&0xFFFF)+ )
void inject_trace(FILE*trace,volatile pxmcc_data_t*data,volatile pmsm_3pmdrv1_t*hw);
void watch_trace(FILE*trace,volatile pxmcc_data_t*data,volatile pmsm_3pmdrv1_t*hw);
volatile uint32_t*ctrl_virt;
volatile uint32_t*trace_virt;
int main(int argc, char *argv[]){
    if(argc<4)FAIL(PARAMERR,100);
    uint32_t ctraddr,traceaddr,words;
    if(
    sscanf(argv[2]," %x",&ctraddr)+
    sscanf(argv[3]," %x",&traceaddr)!=2)FAIL(PARAMERR,100);
    if((ctrl_virt=map_phys_address(ctraddr,300,0))==NULL)FAIL("Could not map dmem to virtual memory.\n",101);
    if((trace_virt=map_phys_address(traceaddr,300,0))==NULL)FAIL("Could not map dmem to virtual memory.\n",101);

    volatile pxmcc_data_t*data=(pxmcc_data_t*)ctrl_virt;
    volatile pmsm_3pmdrv1_t*hw=(pmsm_3pmdrv1_t*)trace_virt;

    printf("%08x %08x\n",ctrl_virt,&(data->axis[0].steps_pos));

    FILE*trace;
    if(strcmp(argv[1],"-i")==0){
        trace=fopen("itrace.csv","w");
        if(trace==NULL)FAIL("Could not open trace file for writing.\n",102);
        inject_trace(trace,data,hw);
        }
    else if(strcmp(argv[1],"-w")==0){
        trace=fopen("wtrace.csv","w");
        if(trace==NULL)FAIL("Could not open trace file for writing.\n",102);    
        watch_trace(trace,data,hw);
        }
    fclose(trace);
    }
void watch_trace(FILE*trace,volatile pxmcc_data_t*data,volatile pmsm_3pmdrv1_t*hw){
    struct timespec request={0,wINTERVAL}; //2kns=2us=nothing
    struct timespec spec;

    fputs("Watch trace;\n",trace);
    fputs("Record number;IRC;IRCx;PWM1;PWM2;PWM3;Q I;D I;ADC1 mcc;ADC2 mcc;ADC3 mcc;Q I cum;D I cum;ADC1 hw;ADC2 hw;ADC3 hw;\n",trace);
    while((hw->pwm1&0x3FFFFFFF)<10&&(hw->pwm2&0x3FFFFFFF)<10&&(hw->pwm3&0x3FFFFFFF)<10);
    clock_gettime(CLOCK_REALTIME,&spec);
    printf("wtrace started at %lld %lld\n",spec.tv_sec,spec.tv_nsec);
    for(int record_num=0;record_num<=8192;record_num++){
        nanosleep(&request,NULL);
        fprintf(trace,"%d;%d;%d;",record_num++,hw->irc_pos,hw->irc_idx_pos);
        fprintf(trace,"%d;%d;%d;",hw->pwm1&0x3FFFFFFF,hw->pwm2&0x3FFFFFFF,hw->pwm3&0x3FFFFFFF);     
        fprintf(trace,"%d;%d;",data->axis[0].cur_dq&0xffff,(data->axis[0].cur_dq>>16)&0xffff);     
        fprintf(trace,"%d;%d;%d;",data->curadc[0].cur_val,data->curadc[1].cur_val,data->curadc[2].cur_val);      
        fprintf(trace,"%d;%d;",data->axis[0].cur_q_cum/INT16_MAX,data->axis[0].cur_d_cum/INT16_MAX);      
        fprintf(trace,"%d;%d;%d;",hw->adc1,hw->adc2,hw->adc3);      
        fputs("\n",trace);
        }    
    clock_gettime(CLOCK_REALTIME,&spec);
    printf("wtrace ended at %lld %lld\n",spec.tv_sec,spec.tv_nsec);      
  }        
void inject_trace(FILE*trace,volatile pxmcc_data_t*data,volatile pmsm_3pmdrv1_t*hw){
  struct timespec request={0,iINTERVAL}; //2kns=2us=nothing
  int record_num=0;
  fputs("Inject trace;\n",trace);
  fputs("Record number;IRC;DQ PWM;PWM1;PWM2;PWM3;Q I;D I;ADC1 mcc;ADC2 mcc;ADC3 mcc;Q I cum;D I cum;ADC1 hw;ADC2 hw;ADC3 hw;\n",trace);
  //smooth spin
  data->axis[0].pwm_dq=50;
  for(int irc=0;irc<=4096;irc+=16){
        data->axis[0].steps_pos=irc;
        fprintf(trace,"%d;%d;%d;",record_num++,irc,50);
        nanosleep(&request,NULL);
        fprintf(trace,"%d;%d;%d;",hw->pwm1&0x3FFFFFFF,hw->pwm2&0x3FFFFFFF,hw->pwm3&0x3FFFFFFF);     
        fprintf(trace,"%d;%d;",data->axis[0].cur_dq&0xffff,(data->axis[0].cur_dq>>16)&0xffff);     
        fprintf(trace,"%d;%d;%d;",data->curadc[0].cur_val,data->curadc[1].cur_val,data->curadc[2].cur_val);      
        fprintf(trace,"%d;%d;",data->axis[0].cur_q_cum/INT16_MAX,data->axis[0].cur_d_cum/INT16_MAX);      
        fprintf(trace,"%d;%d;%d;",hw->adc1,hw->adc2,hw->adc3);      
        fputs("\n",trace);
        }    
    //spin with magnitude increase    
/*    for(int irc=0;irc<=4096;irc+=16){
        data->axis[0].steps_pos=irc;
        int pwm=MIN(irc>>2,100);
        data->axis[0].pwm_dq=pwm;
        fprintf(trace,"%d;%d;%d;",record_num++,irc,pwm);
        nanosleep(&request,NULL);
        fprintf(trace,"%d;%d;%d",hw->pwm1&0x3FFFFFFF,hw->pwm2&0x3FFFFFFF,hw->pwm3&0x3FFFFFFF);     
        fputs("\n",trace);
        }   
    //reverse
    for(int irc=-2048;irc<=2048;irc+=16){
        int abs=irc>0?irc:-irc;
        data->axis[0].steps_pos=abs;
        int pwm=MIN(abs>>2,100);
        data->axis[0].pwm_dq=pwm;
        fprintf(trace,"%d;%d;%d;",record_num++,abs,pwm);
        nanosleep(&request,NULL);
        fprintf(trace,"%d;%d;%d",hw->pwm1&0x3FFFFFFF,hw->pwm2&0x3FFFFFFF,hw->pwm3&0x3FFFFFFF);     
        fputs("\n",trace);
        }       */
  }    