#### software/c/buildc.sh - Software compilation
Run `make CFILE=<path_to_c_file>` to compile a single-file C program. It produces following files:
- `software/out/vhdl/<output>.vhd`, for `-t` mode.
- `software/out/rdwrmem/<output>.txt`, to be loaded on board.
- `software/out/hexstring/rvapo_mem_content.txt`, for `-f` mode.
- `software/out/instrdump/<output>.txt` human-readable instruction dump.
- `software/out/ice40/<output>.vhd` for initialising the ice40 memory.

If you want more than one-line program, copy the `Makefile` in this folder and fill the `SOURCES` variable to your taste.
An example of this can be seen in `./firmware_fresh/`. 

When writing new software, make sure to use the same structure as the existing one - that is, put asm block containing `ebreak` instruction at the end, otherwise the testbench will never terminate. You can move results to a register of memory address your choice, to find them more easily in ghw output. See `/software/c/fibonacci` for minimal example.

See comments in the script for explanation of its function.

#### software/c/riscv32-bare.ld - linker script
First word in memory must be stack address and second one initial PC. This linker script makes it so.
It can also be used to make sure certain variables and functions are at the correct address for interaction with the ARM. Examples are in `./firmware_fresh/` and `./interrupt/`.

#### software/c/crt0local.S - initialiser
Initialises global pointer and stack pointer. The processor actually initialises stack pointer on its own with the first word in memory, but this only for barebones operation.

#### software/c/convertor.c - software formatting
Used by `buildc.sh` to convert output of gcc into formats accepted by the various methods of running rvapo.
Prints help if no argument.