/*******************************************************************
  Components for embedded applications builded for
  laboratory and medical instruments firmware

  firmware.c - multi axis motion controller coprocessor
               firmware for FPGA tumble CPU of lx-rocon system

  (C) 2001-2014 by Pavel Pisa pisa@cmp.felk.cvut.cz
  (C) 2002-2014 by PiKRON Ltd. http://www.pikron.com

  This file can be used and copied according to next
  license alternatives
   - GPL - GNU Public License
   - other license provided by project originators

 *******************************************************************/

#define SUPPRESS_CONDITIONALS 1
#undef  COMPUTE_PHASE_SECTOR
#define WRITE_TRACE
//#define SUMINT
#include <stdint.h>
#include "pxmcc_types.h"
#include "tumbl_addr.h"

#define C_SW(addr,offset)		(*(volatile uint32_t*)(addr+offset))	
#define C_LW(addr,offset)		*((volatile uint32_t*)(addr+offset))
#define PWMLIM(a)           (a)//(a&0xFF)
/* 1 / sqrt(3) * 65536 */
#define RECI16_SQRT3  37837
#define FP2FP(a)      (int32_t)((a)*((double)0x10000))//convert to 16.16 fixed point
int32_t calibration_matrix[2][2]={{FP2FP(0.968973),FP2FP(-0.001363)},{FP2FP(-0.002462),FP2FP(0.967489)}}; //original
//{{FP2FP(1.032024194218360003),FP2FP(0.0014539172814570756713)},//inverted, is almost certainly wrong since real currents should be lower than measured?
//{FP2FP(0.0026262247593157155559),FP2FP(1.0336071826597996952)}};

#define offsbychar(_ptr, _offs) \
   ((typeof(&(_ptr)[0]))((char*)(_ptr) + (_offs)))

volatile pxmcc_data_t pxmcc_data={
    .common={
        .fwversion=PXMCC_FWVERSION,
        .pwm_cycle=5000,
        .min_idle=0x7fff,
        .irc_base=(uint32_t)FPGA_IRC0
    },
    .axis={
        {
        .mode=PXMCC_MODE_BLDC,
        .inp_info=(uint32_t)FPGA_IRC0,
        .ptreci=(1ll<<32)/1000,
        .pwmtx_info=0x00060504
        }
    },
};

void init_defvals(void){}

void main(void){
    uint32_t last_rx_done_sqn = 0;  //this is last adc measure count
    pxmcc_axis_data_t *pxmcc;
    int curtrace_offset=0;

  //  pxmcc_data.common.pwm_cycle = 2500;
  //  pxmcc_data.common.min_idle = 0x7fff;
  //  pxmcc_data.common.irc_base = (uint32_t)FPGA_IRC0;
    pxmcc_data.axis[0].inp_info=(uint32_t)0x43c20008;//&pxmcc_data.axis[0].steps_pos;

    pxmcc = pxmcc_data.axis;
    C_SW(0x1c,0)=(uint32_t)(void*)&pxmcc_data;
    int offset=0;
    do {
        C_SW(0x20,offset)=(uint32_t)(void*)pxmcc;
        offset+=4;
        pxmcc->ccflg  = 0xAAAA;
     //   pxmcc->mode   = PXMCC_MODE_IDLE;
        pxmcc->ptirc  = 1000;//irc pulses per "rotation"
        pxmcc->ptreci = 4294967; /* (1LL<<32)*ptper/ptirc */
        pxmcc->pwm_dq = 0;
        pxmcc++;
    } while(pxmcc != pxmcc_data.axis + PXMCC_AXIS_COUNT);

    do{
    pxmcc = pxmcc_data.axis;
    do{
        uint32_t irc;
        uint32_t ofs = pxmcc->ptofs;
        uint32_t per = pxmcc->ptirc;
        int32_t  pti;
        uint32_t pta;
        uint32_t pwmtx_info;
        volatile uint32_t *uptr;

        pxmcc_data.common.watchdog_counter--;
        if(pxmcc_data.common.watchdog_reset==WATCHDOG_RESET){
          pxmcc_data.common.watchdog_counter=WATCHDOG_TIMEOUT;
          pxmcc_data.common.watchdog_reset=0;
          }
        else if(pxmcc_data.common.watchdog_counter<0){
          pwmtx_info = pxmcc->pwmtx_info;//FPGA_LX_MASTER_TX_PWM0 is replaced by this
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  0) & 0xff);
          *uptr = 0;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  8) & 0xff);
          *uptr = 0;
          uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >> 16) & 0xff);
          *uptr = 0;
          continue;
          }
        irc = *(uint32_t*)pxmcc->inp_info;

        //Transform IRC into phase table index: 1. substract offset
        pti = irc - ofs;
        /*
        * the '>=' is appropriate to keep pti in <0;per-1> range,
        * but sine and cosine computations can work over multiple
        * periods and value of per=0xffffffff allows to control/stop
        * updates if only '>' is used.
        */
        if ((uint32_t)pti > per) {//2. add/sub per(irc count in phase table) until pti is in phase table
          if (pti < 0) {
            ofs -= per;
          } else {
            ofs += per;
          }
          pti = irc - ofs;
          pxmcc->ptofs = ofs;
        }
        pxmcc->ptindx = pti;

        pta = pti * pxmcc->ptreci;//scale pti to 32b

        *FPGA_FNCAPPROX_SIN = pta;//the phase table value is actually computed
        asm volatile("nop\n");
        asm volatile("nop\n");
        asm volatile("nop\n");
        pxmcc->ptsin = *FPGA_FNCAPPROX_SIN;
        asm volatile("nop\n");
        pxmcc->ptcos = *FPGA_FNCAPPROX_COS;

        if (pxmcc->ccflg) {//if active (flag=0 is disable)
            int32_t pwm_alp;
            int32_t pwm_bet;
            int32_t pwm_bet_div_2_k3;
            uint32_t pwm1;
            uint32_t pwm2;
            uint32_t pwm3;
            uint32_t pwm4;
            int32_t pwm_d;
            int32_t pwm_q;
            #if defined(COMPUTE_PHASE_SECTOR) || !defined(SUPPRESS_CONDITIONALS)
            uint32_t phs;
            #endif /*COMPUTE_PHASE_SECTOR*/

            pwm_d = (volatile uint32_t)pxmcc->pwm_dq;//decode command from current ctrl=higher level
            pwm_q = (pwm_d << 16) >> 16;
            pwm_d >>= 16;

            pwm_alp = pwm_d * pxmcc->ptcos - pwm_q * pxmcc->ptsin;//here begins some actual computation, this looks to be matrix mul
            pwm_bet = pwm_d * pxmcc->ptsin + pwm_q * pxmcc->ptcos;

            if (pxmcc->mode == PXMCC_MODE_BLDC) {//BLDC mode, appears to be default (0)
                pwm_bet_div_2_k3 = RECI16_SQRT3 * (pwm_bet >> 16);

              #ifndef SUPPRESS_CONDITIONALS
              if (pwm_bet > 0)
                  if (pwm_alp > 0)
                  /* pwm_bet > 2 * k3 * pwm_alp */
                    if (pwm_bet_div_2_k3 > pwm_alp)
                      phs = 1;
                    else
                      phs = 0;
                  else
                    /* -pwm_bet > 2 * k3 * pwm_alp */
                    if (pwm_bet_div_2_k3 < -pwm_alp)
                      phs = 2;
                    else
                      phs = 1;
                else
                  if (pwm_alp > 0)
                    /* pwm_bet > -2 * k3 * pwm_alp */
                    if (pwm_bet_div_2_k3 > -pwm_alp)
                      phs = 5;
                    else
                      phs = 4;
                  else
                    /* pwm_bet > 2 * k3 * u_alp */
                    if (pwm_bet_div_2_k3 > pwm_alp)
                      phs = 3;
                    else
                      phs = 4;

                if (phs <= 1) {
                  /* pwm1 = pwm_alp + 1.0/(2.0*k3) * pwm_bet */
                  pwm1 = (pwm_alp + pwm_bet_div_2_k3) >> 16;
                  /* pwm2 = 1/k3 * pwm_bet */
                  pwm2 = pwm_bet_div_2_k3 >> 15;
                  pwm3 = 0;
                } else if (phs <= 3) {
                  pwm1 = 0;
                  /* pwm2 = 1.0/(2.0*k3) * pwm_bet - pwm_alp */
                  pwm2 = (pwm_bet_div_2_k3 - pwm_alp) >> 16;
                  /* pwm3 = -1.0/(2.0*k3) * pwm_bet - pwm_alp */
                  pwm3 = (-pwm_bet_div_2_k3 - pwm_alp) >>16;
                } else {
                  /* pwm1 = pwm_alp - 1.0/(2.0*k3) * pwm_bet */
                  pwm1 = (pwm_alp - pwm_bet_div_2_k3) >>16;
                  pwm2 = 0;
                  /* pwm3 = -1/k3 * pwm_bet */
                  pwm3 = -pwm_bet_div_2_k3 >> 15;
                }
                #else /*SUPPRESS_CONDITIONALS*/
                {
                int32_t alp_m_bet_d2k3;
                uint32_t state23_msk;
                uint32_t pwm23_shift;
                uint32_t bet_sgn = pwm_bet >> 31;
                uint32_t alp_sgn = pwm_alp >> 31;
                #ifdef COMPUTE_PHASE_SECTOR
                uint32_t bet_sgn_cpl = ~bet_sgn;
                #endif /*COMPUTE_PHASE_SECTOR*/

                alp_m_bet_d2k3 = (alp_sgn ^ pwm_alp) - (bet_sgn ^ (pwm_bet_div_2_k3 + bet_sgn));
                alp_m_bet_d2k3 = alp_m_bet_d2k3 >> 31;

                state23_msk = alp_sgn & ~alp_m_bet_d2k3;

                /*
                  *   bet alp amb s23
                  *    0   0   0   0 -> 0 (000)
                  *    0   0  -1   0 -> 1 (001)
                  *   -1   0  -1   0 -> 1 (001)
                  *   -1   0   0  -1 -> 2 (010)
                  *   -1  -1   0  -1 -> 3 (011)
                  *   -1  -1  -1   0 -> 4 (100)
                  *    0  -1  -1   0 -> 4 (100)
                  *    0  -1   0   0 -> 5 (101)
                  */

                #ifdef COMPUTE_PHASE_SECTOR
                phs = (bet_sgn & 5) + (bet_sgn_cpl ^ ((alp_m_bet_d2k3 + 2 * state23_msk) + bet_sgn_cpl));
                #endif /*COMPUTE_PHASE_SECTOR*/

                pwm1 = pwm_alp & state23_msk;
                pwm2 = pwm_bet_div_2_k3 - pwm1;
                pwm3 = -pwm_bet_div_2_k3 - pwm1;
                pwm2 &= (~bet_sgn | state23_msk);
                pwm3 &= (bet_sgn | state23_msk);
                pwm1 = pwm_alp + pwm2 + pwm3;
                pwm1 &= ~state23_msk;
                pwm1 >>= 16;
                pwm23_shift = 15 - state23_msk;
                pwm2 >>= pwm23_shift;
                pwm3 >>= pwm23_shift;
                }//this is end of define block, unconditional
                #endif /*SUPPRESS_CONDITIONALS*/

                #ifdef COMPUTE_PHASE_SECTOR
                pxmcc->ptphs = phs;
                #endif /*COMPUTE_PHASE_SECTOR*/

                #if 0
                *FPGA_LX_MASTER_TX_PWM0 = pwm2 | 0x4000;
                *FPGA_LX_MASTER_TX_PWM1 = pwm3 | 0x4000;
                *FPGA_LX_MASTER_TX_PWM2 = pwm1 | 0x4000;
                #else//having computed pwm, proceed to apply it
                pwmtx_info = pxmcc->pwmtx_info;//FPGA_LX_MASTER_TX_PWM0 is replaced by this
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  0) & 0xff);
                pxmcc->pwm_prew[1] = *uptr & 0x3fff;//save the current pwm, max 14 bits, this corresponds to pmsm driver value
                *uptr = PWMLIM(pwm2) | FPGA_PWM_ENABLE_MASK;//set enable?
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  8) & 0xff);
                pxmcc->pwm_prew[2] = *uptr & 0x3fff;
                *uptr = PWMLIM(pwm3) | FPGA_PWM_ENABLE_MASK;
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >> 16) & 0xff);
                pxmcc->pwm_prew[0] = *uptr & 0x3fff;
                *uptr = PWMLIM(pwm1) | FPGA_PWM_ENABLE_MASK;
                #endif//END of BLCD mode
                }
            else {//when not BLCD, use 4 axes
                uint32_t alp_sgn = pwm_alp >> 31;
                uint32_t bet_sgn = pwm_bet >> 31;
                pwm_alp >>= 16;
                pwm_bet >>= 16;
                pwm1 = -pwm_alp & alp_sgn;
                pwm2 = pwm_alp & ~alp_sgn;
                pwm3 = -pwm_bet & bet_sgn;
                pwm4 = pwm_bet & ~bet_sgn;

                pwmtx_info = pxmcc->pwmtx_info;
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  0) & 0xff);
                pxmcc->pwm_prew[0] = *uptr & 0x3fff;
                *uptr = pwm1 | 0x4000;
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  8) & 0xff);
                pxmcc->pwm_prew[1] = *uptr & 0x3fff;
                *uptr = pwm2 | 0x4000;
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >> 16) & 0xff);
                pxmcc->pwm_prew[2] = *uptr & 0x3fff;
                *uptr = pwm3 | 0x4000;
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >> 24) & 0xff);
                pxmcc->pwm_prew[3] = *uptr & 0x3fff;
                *uptr = pwm4 | 0x4000;
              /*   if (pxmcc->mode == PXMCC_MODE_STEPPER) {
                if ((pxmcc->steps_sqn_next ^ last_rx_done_sqn) & 0x1f) {//if lower seven bits changed?
                    pxmcc->steps_pos += pxmcc->steps_inc;
                } else {
                    pxmcc->steps_pos = pxmcc->steps_pos_next;
                    pxmcc->steps_inc = pxmcc->steps_inc_next;
                    pxmcc->steps_inc_next = 0;
                }
                }*/
                }
            }//end if active 
        else {//if not active, then do not update pwm, just read current value
            if(pxmcc->mode == PXMCC_MODE_BLDC) {
                pwmtx_info = pxmcc->pwmtx_info;
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  0) & 0xff);
                pxmcc->pwm_prew[1] = *uptr & 0x3fff;
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  8) & 0xff);
                pxmcc->pwm_prew[2] = *uptr & 0x3fff;
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >> 16) & 0xff);
                pxmcc->pwm_prew[0] = *uptr & 0x3fff;
                } 
            else {
                pwmtx_info = pxmcc->pwmtx_info;
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  0) & 0xff);
                pxmcc->pwm_prew[0] = *uptr & 0x3fff;
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >>  8) & 0xff);
                pxmcc->pwm_prew[1] = *uptr & 0x3fff;
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >> 16) & 0xff);
                pxmcc->pwm_prew[2] = *uptr & 0x3fff;
                uptr = FPGA_PMSM_3PMDRV_PTR + ((pwmtx_info >> 24) & 0xff);
                pxmcc->pwm_prew[3] = *uptr & 0x3fff;
                }
        }
        pxmcc++;//move to next axis
        } while(pxmcc != pxmcc_data.axis + PXMCC_AXIS_COUNT);//thus is pwm for all axes updated

    //original firmware proceeds to synchronise using last_rx_done_sqn and FPGA_LX_MASTER_RX_DDIV, lines 341-356
    //it also records the shortest sync time
    {//this is the sync block
      uint32_t idlecnt = 0;
      uint32_t sqn;
      do {
        sqn = FPGA_STAT_GET_COUNT(*FPGA_STAT_REG);
        idlecnt++;
        } while ((sqn == last_rx_done_sqn)||(sqn>0xFFF));
      pxmcc_data.common.act_idle = idlecnt;
      if ((idlecnt < pxmcc_data.common.min_idle)){// && last_rx_done_sqn) {
          pxmcc_data.common.min_idle = idlecnt;
          }
      last_rx_done_sqn = sqn;//masking out the desync bit is not necessary because we require it to be 0
      pxmcc_data.common.rx_done_sqn = last_rx_done_sqn;
      asm volatile("": : : "memory");
    }

    //after that it saves adc values (=current on engine coils) into pxmcc_curadc_data_t
    volatile pxmcc_curadc_data_t *curadc = pxmcc_data.curadc;
    volatile uint32_t *siroladc = FPGA_PMSM_3PMDRV_ADC0;
    uint32_t val;

    for(int i = 0; i < PXMCC_CURADC_CHANNELS;i++ ) {
        val = *siroladc;

        curadc->cur_val = (uint16_t)(val - curadc->siroladc_last) -curadc->siroladc_offs;
        curadc->siroladc_last = val;

        curadc++;
        siroladc++;
        }//*/

    //finally it computes dq current
    pxmcc = pxmcc_data.axis;
    do {//second loop over all axes, calculates dq current from hall adc and maybe something else
      int32_t  cur_alp;
      int32_t  cur_bet;
      int32_t  cur_d;
      int32_t  cur_q;
      uint32_t pwm1;
      uint32_t pwm2;
      uint32_t pwm3;
      int32_t  cur1;
      int32_t  cur2;
      int32_t  cur3;
      volatile int32_t  *pcurmult;
      uint32_t curmult_idx;
      uint32_t pwm_reci;
      uint32_t out_info;
     #if defined(COMPUTE_PHASE_SECTOR) || !defined(SUPPRESS_CONDITIONALS)
      uint32_t phs;
     #ifdef COMPUTE_PHASE_SECTOR
      phs = pxmcc->ptphs;
     #endif /*COMPUTE_PHASE_SECTOR*/
     #endif /*COMPUTE_PHASE_SECTOR*/

      out_info = pxmcc->out_info;
      if (pxmcc->mode == PXMCC_MODE_BLDC) {

        pwm1 = pxmcc->pwm_prew[0];
        pwm2 = pxmcc->pwm_prew[1];
        pwm3 = pxmcc->pwm_prew[2];//read previous pwm value

       #ifndef SUPPRESS_CONDITIONALS
        #ifndef COMPUTE_PHASE_SECTOR
        if (pwm1 > pwm2)
          if (pwm2 > pwm3)
            phs = 0;
          else if (pwm1 > pwm3)
            phs = 5;
          else
            phs = 4;
        else
          if (pwm2 < pwm3)
            phs = 3;
          else if (pwm1 < pwm3)
            phs = 2;
          else
            phs = 1;
        #endif /*COMPUTE_PHASE_SECTOR*/

        curmult_idx = (0x00201201 >> (4 * phs)) & 3;
        pwm_reci = pxmcc_data.common.pwm_cycle - pxmcc->pwm_prew[curmult_idx];
        pwm_reci = (pxmcc_data.common.pwm_cycle << 16) / pwm_reci;

        /*
         * Translate index from pwm1, pwm2, pwm3 order to
         * to order of current sources 0->2 1->0 2->1
         *
         * This solution modifies directly value in pxmcc_curadc_data_t
         * so it is destructive and has not to be applied twice,
         * but it is much better optimized
         */
        curmult_idx = (0x102 >> (curmult_idx * 4)) & 3;
        pcurmult = &pxmcc_data.curadc[out_info + curmult_idx].cur_val;
        *pcurmult = (int32_t)(pwm_reci * (*pcurmult)) >> 16;

        cur2 = pxmcc_data.curadc[out_info + 0].cur_val;
        cur3 = pxmcc_data.curadc[out_info + 1].cur_val;
        cur1 = pxmcc_data.curadc[out_info + 2].cur_val;

        if ((phs == 5) || (phs == 0))
          cur_alp = -(cur2 + cur3);
        else
          cur_alp = cur1;

        if ((phs == 5) || (phs == 0))
          cur_bet = cur2 - cur3;
        else if ((phs == 3) || (phs == 4))
          cur_bet = 2 * cur2 + cur1;
        else /* 1 2 */
          cur_bet = -(2 * cur3 + cur1);
       #else /*SUPPRESS_CONDITIONALS*/
        {
          /*
           *   u1>u2 u2>u3 u1>u3               cm
           *     0     1     1 ->  1 (1, 0, 2) 0
           *     0     1     0 ->  2 (1, 2, 0) 2
           *     0     0     0 ->  3 (2, 1, 0) 1
           *     1     0     0 ->  4 (2, 0, 1) 0
           *     1     0     1 ->  5 (0, 2, 1) 2
           *     1     1     1 ->  0 (0, 1, 2) 1
           */

          uint32_t u1gtu2 = (int32_t)(pwm2 - pwm1) >> 31;
          uint32_t u1gtu3 = (int32_t)(pwm3 - pwm1) >> 31;
          uint32_t u2gtu3 = (int32_t)(pwm3 - pwm2) >> 31;
          uint32_t state50_msk = u1gtu2 & u1gtu3;
          uint32_t pwm_reci_bits;
          uint32_t sz4idx = sizeof(*pxmcc->pwm_prew);
          uint32_t sz4cur = sizeof(*pxmcc_data.curadc);
          uint32_t curmult_idx2curadc;

         #if 0
          curmult_idx = (((u1gtu3 ^ u1gtu2) | 1) ^ u2gtu3 ^ u1gtu2) & 3;
          pwm_reci = pxmcc_data.common.pwm_cycle - pxmcc->pwm_prew[curmult_idx];
         #else
          /* Variant where curmult_idx is directly computed as byte offset to uint32_t array */
          curmult_idx = (((u1gtu3 ^ u1gtu2) | (1 * sz4idx)) ^ u2gtu3 ^ u1gtu2) & (3 * sz4idx);
          pwm_reci = pxmcc_data.common.pwm_cycle - *offsbychar(pxmcc->pwm_prew, curmult_idx);
         #endif

         #ifdef SUMINT //this is not used when hall sensors measure actuall coil current
         #if 0
          pwm_reci_bits = __builtin_clzl(pwm_reci);
         #else
          //asm("clz %0,%1\n":"=r"(pwm_reci_bits):"r"(pwm_reci));
          asm volatile("div %0,%0,%1\n":"=r"(pwm_reci_bits):"r"(pwm_reci));
         #endif         
          pwm_reci <<= pwm_reci_bits;
          *FPGA_FNCAPPROX_RECI = pwm_reci;
          asm volatile("nop\n");
          asm volatile("nop\n");
          asm volatile("nop\n");
          pwm_reci = *FPGA_FNCAPPROX_RECI;
          pwm_reci >>= 16;
          pwm_reci *= pxmcc_data.common.pwm_cycle;
          pwm_reci >>= 30 - pwm_reci_bits;
        #endif
          /*
           * Translate index from pwm1, pwm2, pwm3 order to
           * to order of current sources 0->2 1->0 2->1
           *
           * This solution modifies directly value in pxmcc_curadc_data_t
           * so it is destructive and has not to be applied twice,
           * but it is much better optimized
           */
         #if 0
          curmult_idx = (0x102 >> (curmult_idx * 4)) & 3;
          pcurmult = &pxmcc_data.curadc[out_info + curmult_idx].cur_val;
         #else
          curmult_idx2curadc = ((2 * sz4cur) << (0 * sz4idx)) |
                               ((0 * sz4cur) << (1 * sz4idx)) |
                               ((1 * sz4cur) << (2 * sz4idx));
          curmult_idx = (curmult_idx2curadc >> curmult_idx) & (sz4cur | (sz4cur << 1));
          pcurmult = &pxmcc_data.curadc[out_info].cur_val;
          pcurmult = offsbychar(pcurmult, curmult_idx);
         #endif
         #ifdef SUMINT
          *pcurmult = (int32_t)(pwm_reci * (*pcurmult)) >> 16;
         #endif
          cur2 = pxmcc_data.curadc[out_info + 0].cur_val;
          cur3 = pxmcc_data.curadc[out_info + 1].cur_val;
          cur1 = pxmcc_data.curadc[out_info + 2].cur_val;

          cur_alp = -(cur2 + cur3);                /* 5 0 */
          cur_alp &= state50_msk;                  /* 1 2 3 4 */
          cur_alp |= cur1 & ~state50_msk;

          cur_bet = (-2 * cur3 - cur1) & u2gtu3;   /* 1 2 */
          cur_bet |= (2 * cur2 + cur1) & ~u2gtu3;  /* 3 4 */
          cur_bet &= ~state50_msk;
          cur_bet |= (cur2 - cur3) & state50_msk;  /* 5 0 */
        }
       #endif /*SUPPRESS_CONDITIONALS*/

        cur_bet *= RECI16_SQRT3;
        cur_bet >>= 16;
      }//calculation for BLCD motor 
      else {//calculation for others
        int32_t bet_pwm = pxmcc->pwm_prew[2];
        uint32_t bet_sgn = (bet_pwm - 1) >> 31;
        int32_t alp_pwm = pxmcc->pwm_prew[0];
        uint32_t alp_sgn = (alp_pwm - 1) >> 31;
        cur_bet = (pxmcc_data.curadc[out_info + 3].cur_val & ~bet_sgn) -
                  (pxmcc_data.curadc[out_info + 2].cur_val & bet_sgn);
        cur_alp = (pxmcc_data.curadc[out_info + 1].cur_val & ~alp_sgn) -
                  (pxmcc_data.curadc[out_info + 0].cur_val & alp_sgn);
      }
      cur_alp=calibration_matrix[0][0]*cur_alp+calibration_matrix[0][1]*cur_bet;//apply calibration matrix
      cur_bet=calibration_matrix[1][0]*cur_alp+calibration_matrix[1][1]*cur_bet;

      cur_d =  cur_alp * pxmcc->ptcos + cur_bet * pxmcc->ptsin;//park transform application
      cur_q = -cur_alp * pxmcc->ptsin + cur_bet * pxmcc->ptcos;

      pxmcc->cur_dq = (cur_d & 0xffff0000) | ((cur_q >> 16) & 0xffff);//set dq currents, which is output 

      pxmcc->cur_d_cum = ((pxmcc->cur_d_cum + (cur_d >> 4)) & ~0x3f)|(last_rx_done_sqn & 0x1f);
      pxmcc->cur_q_cum = ((pxmcc->cur_q_cum + (cur_q >> 4)) & ~0x3f)|(last_rx_done_sqn & 0x1f);

      pxmcc++;
    } while(pxmcc != pxmcc_data.axis + PXMCC_AXIS_COUNT);

    #ifdef WRITE_TRACE
    if(pxmcc_data.common.trace_address==0)continue;
    if(curtrace_offset>=pxmcc_data.common.trace_size){
      pxmcc_data.common.trace_address=0;
      curtrace_offset=0;  
      continue;
      }
    *IO32ADDR(pxmcc_data.common.trace_address+curtrace_offset+0)=pxmcc_data.curadc[0].cur_val;  
    *IO32ADDR(pxmcc_data.common.trace_address+curtrace_offset+4)=pxmcc_data.curadc[1].cur_val;  
    *IO32ADDR(pxmcc_data.common.trace_address+curtrace_offset+8)=pxmcc_data.curadc[2].cur_val;  
    *IO32ADDR(pxmcc_data.common.trace_address+curtrace_offset+12)=0x12344321;  
    curtrace_offset+=16;  
    #endif
    }while(1);//main loop
}
