/* LX ROCON firmware startup file */

.globl _main
.align 2

_main:

	/* Stack pointer */
	addi     r1, r0, 0xFFC

	/* Other register values are not initialized to 0 */
	addi     r2, r0, 0
	addi     r3, r0, 0
	addi     r4, r0, 0
	addi     r5, r0, 0
	addi     r6, r0, 0
	addi     r7, r0, 0
	addi     r8, r0, 0
	addi     r9, r0, 0
	addi     r10, r0, 0
	addi     r11, r0, 0
	addi     r12, r0, 0
	addi     r13, r0, 0
	addi     r14, r0, 0
	addi     r15, r0, 0
	addi     r16, r0, 0
	addi     r17, r0, 0
	addi     r18, r0, 0
	addi     r19, r0, 0
	addi     r20, r0, 0
	addi     r21, r0, 0
	addi     r22, r0, 0
	addi     r23, r0, 0
	addi     r24, r0, 0
	addi     r25, r0, 0
	addi     r26, r0, 0
	addi     r27, r0, 0
	addi     r28, r0, 0
	addi     r29, r0, 0
	addi     r30, r0, 0
	addi     r31, r0, 0

	/* reset data */
	addi     r6, r0, _sdata
	addi     r7, r0, _edata
	rsub     r18, r6, r7
	brci     le, r18, .Lenddata
.Lloopdata:
	swi      r0, r6, 0
	addi     r6, r6, 4
	rsub     r18, r6, r7
	brci     gt, r18, .Lloopdata
.Lenddata:
	/* Init default values */
	brli     r15, init_defvals

	/* Run program */
	brli     r15, main

	/* End of program */
	halt     0
