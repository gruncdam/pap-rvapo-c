/* minimal replacement of crt0.o which is else provided by C library */

.globl _irq_handler

.option norelax

.text
.section .text._irq_handler
.weak interrupt0,interrupt1,interrupt2,interrupt3  //this does not define any symbols, just marks them as weak
interrupt0://these definitions can now be overriden by C functions
interrupt1://but of course no documentation mentions this second step
interrupt2:
interrupt3://the hw maps irq pins to rvapo_ctrl::ymax, which is at 0x43c00020 (reg8, lower 16b)
_irq_handler://generic handler address is on rvapo_ctrl::arm_base, which si at 0x43c00018 (reg6) 
	.option norelax
	//sw		x2,-120(sp)	//save stack pointer before modification
	addi 	sp,sp,-136//-128
	addi	ra,ra,-4

	sw 		x1,4(sp)
	//sw 		x2,8(sp)
	sw 		x3,0xC(sp)
	sw 		x4,0x10(sp)
	sw 		x5,0x14(sp)
	sw 		x6,0x18(sp)
	sw 		x7,0x1C(sp)
	sw 		x8,0x20(sp)
	sw 		x9,0x24(sp)
	//sw 		x10,0x28(sp)	a0
	sw 		x11,0x7C(sp)
	sw 		x12,0x2C(sp)
	sw 		x13,0x30(sp)
	sw 		x14,0x34(sp)
	sw 		x15,0x38(sp)
	sw 		x16,0x3C(sp)
	sw 		x17,0x40(sp)
	sw 		x18,0x44(sp)
	sw 		x19,0x48(sp)
	sw 		x20,0x4C(sp)
	sw 		x21,0x50(sp)
	sw 		x22,0x54(sp)
	sw 		x23,0x58(sp)
	sw 		x24,0x5C(sp)
	sw 		x25,0x60(sp)
	sw 		x26,0x64(sp)
	sw 		x27,0x68(sp)
	sw 		x28,0x6C(sp)
	sw 		x29,0x70(sp)
	sw 		x30,0x74(sp)
	sw 		x31,0x78(sp)

	lw 		a0,0x10(a0)
	jalr	ra,a0,0

	lw 		x1,4(sp)
	//lw 		x2,8(sp)
	lw 		x3,0xC(sp)
	lw 		x4,0x10(sp)
	lw 		x5,0x14(sp)
	lw 		x6,0x18(sp)
	lw 		x7,0x1C(sp)
	lw 		x8,0x20(sp)
	lw 		x9,0x24(sp)
	//lw 		x10,0x28(sp)	a0
	lw 		x11,0x7C(sp)
	lw 		x12,0x2C(sp)
	lw 		x13,0x30(sp)
	lw 		x14,0x34(sp)
	lw 		x15,0x38(sp)
	lw 		x16,0x3C(sp)
	lw 		x17,0x40(sp)
	lw 		x18,0x44(sp)
	lw 		x19,0x48(sp)
	lw 		x20,0x4C(sp)
	lw 		x21,0x50(sp)
	lw 		x22,0x54(sp)
	lw 		x23,0x58(sp)
	lw 		x24,0x5C(sp)
	lw 		x25,0x60(sp)
	lw 		x26,0x64(sp)
	lw 		x27,0x68(sp)
	lw 		x28,0x6C(sp)
	lw 		x29,0x70(sp)
	lw 		x30,0x74(sp)
	lw 		x31,0x78(sp)

	addi	sp,sp,128
	lw 		a0,4(sp)	
	addi	sp,sp,8

	ebreak

	nop//rvapo executes some instructions after ebreak or ecall before it reaches M, so these nops are necessary
	nop
	nop