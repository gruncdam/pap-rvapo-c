
int main(){
	int val1=0x12345678;
	int val2=-10;//FFFFFFF6
	int val3=-7003;//FFFFE4A5
	asm volatile ( 
		//setup
		"li t0, 0x7;"
		"li t1, 0x5;"
		"li t2, 0x3;"
		"li t3, 0x100;"
		"sw t0, 0xC(zero);"
		"sw t1, 4(zero);"
		"sw t2, 8(zero);"
		"nop;"
		"nop;"
		"nop;"
		"nop;"
		"div s0,%0,t2;"//signed operation on unsigned numbers
		"sw s0, 0x10(zero);"
		"rem s0,%0,t2;"
		"sw s0, 0x14(zero);"
		"mul s0,%0,t2;"
		"sw s0, 0x18(zero);"
		"nop;"
		"nop;"
		"div s0,%2,%1;"//signed operation on signed numbers
		"sw s0, 0x20(zero);"
		"rem s0,%2,%1;"
		"sw s0, 0x24(zero);"
		"mul s0,%2,%1;"
		"sw s0, 0x28(zero);"
		"nop;"
		"nop;"
		"divu s0,%1,%2;" //unsigned operation on signed numbers
		"sw s0, 0x30(zero);"
		"remu s0,%1,%2;"
		"sw s0, 0x34(zero);"
		"mulhu s0,%2,%1;"
		"sw s0, 0x38(zero);"
		"nop;"
		"nop;"
		"div s0,%2,t0;"//signed operation on signed/unsigned numbers
		"sw s0, 0x40(zero);"
		"rem s0,%2,t0;"
		"sw s0, 0x44(zero);"
		"mul s0,%2,t1;"
		"sw s0, 0x48(zero);"
		"nop;"
		"nop;"
		"divu s0,%0,t2;" //unsigned operation on unsigned numbers
		"sw s0, 0x50(zero);"
		"remu s0,%0,t2;"
		"sw s0, 0x54(zero);"
		"mulhu s0,%0,t3;"//mulHu!
		"sw s0, 0x58(zero);"
		"nop;"
		"nop;"
		"mulh s0,%0,%2;"//mulh FFFFFE0E02469B56
		"sw s0, 0x60(zero);"
		"mulh s0,%0,t3;"
		"sw s0, 0x64(zero);"
		"mulhsu s0,t0,%1;"
		"sw s0, 0x68(zero);"
		"nop;"
		"nop;"
		"remu s0,%2,%1;"//indivisble
		"sw s0, 0x70(zero);"
		"mul s0,t0,t0;"//back to back mul-state machine restart
		"mul s0,s0,t0;"
		"sw s0, 0x74(zero);"
		"div s0,t3,t2;"
		"div s0,s0,t2;"
		"sw s0, 0x78(zero);"
		"ebreak;"
			:
		    : "r" (val1),"r" (val2),"r" (val3)	//*/
		);
	}	
/*	Expected memory dump:
0000   : 00 00 	10 00
0004   : 00 00  00 05
0008   : 00 00  00 03
000C   : 00 00  00 07
0010   : 06 11  72 28
0014   : 00 00  00 00
0018   : 36 9d  03 68

0020   : 00 00  02 BC
0024   : 00 00  00 03
0028   : 00 01  11 8E

0030   : 00 00  00 01
0034   : 00 00  1B 51
0038   : FF FF  E4 9B //is mulh; mulu=lower on signed is same as mul=lower on the same

0040   : FF FF  FC 18 //-1000
0044   : 00 00  00 03 
0048   : ff ff  77 39 

0050   : 06 11  72 28
0054   : 00 00  00 00
0058   : 00 00  00 12

0060   : FF FF  FE 0E
0064   : 00 00  00 12
0068   : 00 00  00 06

0070   : FF FF  E4 A5
0074   : 00 00  01 57
0078   : 00 00  00 1C
*/  

