int main(){
    asm volatile(
        "addi s3,zero,-4;"//s3 is -4
        "sw s3, 0x10(zero);"
        "nop;"  

        "addi s0,zero,45;"//s0 is 45
        "sw s0, 0x14(zero);"
        "nop;"

        "add s1,s0,s0;"//s1 is 90
        "sw s1, 0x18(zero);"
        "nop;"

        "slli s2,s3,4;"//s2 is -64
        "sw s2, 0x20(zero);"
        "nop;"

        "srai s2,s2,2;"//s2 is -16
        "sw s2, 0x24(zero);"
        "nop;"

        "srli s2,s2,2;"//s2 is 3FFFFFFC
        "sw s2, 0x28(zero);"
        "nop;"

        "sub s2,s2,s1;"//3FFFFFA2  (-90)
        "sw s2, 0x2C(zero);"
        "nop;"

        "slti s0,s3,-8;"//-8 is less
        "sw s0, 0x30(zero);"
        "nop;"

        "slti s0,s3,8;"//8 greater
        "sw s0, 0x34(zero);"
        "nop;"

        "sltiu s0,s3,45;"//45<-4 cause unsigned
        "sw s0, 0x38(zero);"
        "nop;"

        "addi t0,zero,-1;"

        "sltu s0,s3,t0;"//-1 > -4 even when unsigned
        "sw s0, 0x3C(zero);"
        "nop;"
        ::);
    }