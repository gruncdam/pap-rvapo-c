#include <stdint.h>

#define PARLCD_REG_BASE_PHYS  ((unsigned char*)0x1000) //0x43c00000 change for fpga
#define PARLCD_REG_SIZE       0x00004000

#define PARLCD_REG_CMD_o                0x0008
#define PARLCD_REG_DATA_o               0x000C

#define SPILED_REG_BASE_PHYS        0x00200000 //wrong
#define SPILED_REG_SIZE             0x00004000

#define SPILED_REG_LED_LINE_o           0x004
#define SPILED_REG_LED_RGB1_o           0x010
#define SPILED_REG_LED_RGB2_o           0x014
#define SPILED_REG_LED_KBDWR_DIRECT_o   0x018       //what is this?

#define SPILED_REG_LED_KBDENC_i         0x020 
#define SPILED_REG_LED_ENC2_i         0x024 

#define AXI_BRAM_BASE           0x80000000	//access bram 3 directly
#define ARMAXI_BRAM_BASE		0x46000000	//access bram 3 through ARM

#define DATA                    0xFAF0FAF0
#define DATA_ORDERED            0x12345678

#define LOOPBACK_BASE           0x40000000
#define LOOPBACK_46_OFFSET      0x06000000

#define DDROCM_BASE             0x00000000 
//#define DDROCM_ALLOC            0x37a1f000	

#define C_SW(addr,offset)		(*(volatile uint32_t*)(addr+offset))	
#define C_LW(addr,offset)		*((volatile uint32_t*)(addr+offset))
#define NOP						asm volatile("nop;":: )

int main(){
	int val=DATA;
	uint32_t axi_base=C_LW(C_LW(4,0),-4);

    uint32_t base=DDROCM_BASE+axi_base;
	asm volatile ( 
		"sw %0, 0x4(%1);"
		"sw %0, 0x10(%1);"
		"nop;"
			:
		    : "r" (val),"r" (base)	//*/
		);//write into ARM RAM using assembler

	uint32_t read=*((volatile uint32_t*)(base + 0x4));
	//(*(volatile uint32_t*)(base + 0x20))=read;
	//(*(volatile uint32_t*)(base + 0x34))=read;//RW ARM RAM using basic C
	uint32_t read2=*((volatile uint32_t*)(base + 0x10));//test 4B align (the ARM AXI is 8B)
	asm volatile ( 
        "sw %0, 0x10(zero);"
		"sw %1, 0x14(zero);"
        "nop;"
			:
		    : "r" (read),"r" (read2)
		);//verify read by a more foolproof method
	 
	base=AXI_BRAM_BASE;//test accessing FPGA BRAM through direct AXI
	C_SW(base,0x4)=DATA;//4B aligned
	C_SW(base,0x10)=DATA;//8B aligned
	uint32_t read3=C_LW(base,0x4);
	uint32_t read4=C_LW(base,0x10);
	C_SW(0,0x20)=read3;//test read
	C_SW(0,0x24)=read4;
	NOP;
	base=ARMAXI_BRAM_BASE;//test accessing FPGA BRAM through ARM AXI
	C_SW(base,0x34)=DATA;
	C_SW(base,0x40)=DATA;
	uint32_t read5=C_LW(base,0x34);
	uint32_t read6=C_LW(base,0x40);
	C_SW(0,0x30)=read5;
	C_SW(0,0x34)=read6;

	//unaligned
	base=DDROCM_BASE+axi_base;
	C_SW(base,0x22)=DATA_ORDERED;
	uint32_t read7=C_LW(base,0x23);
	(*(volatile uint8_t*)(base+0x2E))=0xCD;
	uint8_t read8=*((volatile uint32_t*)(base+0x2E));
	C_SW(0,0x40)=read7;	
	C_SW(0,0x44)=(uint32_t)read8;	
	}	
