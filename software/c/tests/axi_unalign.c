int main(){
	int base=0x1000;
	int val=0xFFFFFFFF;
	asm volatile ( 
		"sw %0, 5(%1);"
		"sw %0, 7(%1);"
		"addi %1, %1, 0x10;"
		"ebreak;"
			:
		    : "r" (val),"r" (base)	//*/
		);
	}	
//		theory					act	
//reg1	=0x4	is led line		0x4
//reg4	=0x10	is rgb1			0x18
//reg5	=0x14
//reg6  =0x18   
