/*
This program test the issue of delayed memory M2W register skipping, which skips the MEM entity that 
edits memory output, causing lhu, lh, lb, lbu to behave like lw.
Unaligned memory access has that resolved, so we test only aligned reads 
(which are broken by this bug even if unalign generic is 1 because that only changes handling of unaligned addresses).
Unaligned reads when unalign generic is 0 are not expected to ever work.
mem_unalign_large supersedes this
*/
int main(){
	int val1=0x12345678;
	int val2=0xABBACDDC;
	asm volatile ( 
		"sw %0, 0(zero);" //setup a memory region
		"sw %1, 4(zero);"

		"lhu s2, (zero);"//nonextensible word
		"nop;"
		"sw s2, 0x20(zero);"
		"nop;"

		"lbu s2, (zero);"
		"nop;"
		"sw s2, 0x24(zero);"
		"nop;"

		"lh s2, (zero);"
		"nop;"
		"sw s2, 0x28(zero);"
		"nop;"

		"lb s2, (zero);"
		"nop;"
		"sw s2, 0x2C(zero);"
		"nop;"

		"lhu s2, 4(zero);"//extensible word but unsigned read
		"nop;"
		"sw s2, 0x40(zero);"
		"nop;"

		"lbu s2, 4(zero);"
		"nop;"
		"sw s2, 0x44(zero);"
		"nop;"

		"lh s2, 4(zero);"//extensible word and signed read
		"nop;"
		"sw s2, 0x48(zero);"
		"nop;"

		"lb s2, 4(zero);"
		"nop;"
		"sw s2, 0x4C(zero);"
		"nop;"

		"ebreak;"
			:
		    : "r" (val1),"r" (val2)	//*/
		);
	}	
/*	Expected memory dump:
0000   : 12 34  56 78
0004   : AB BA  CD DC
0008   : 00 00  00 00
000C   : 00 00  00 00
0010   : 00 00  00 00
0014   : 00 00  00 00
0018   : 00 00  00 00
001C   : 00 00  00 00
0020   : 00 00  56 78
0024   : 00 00  00 78
0028   : 00 00  56 78
002C   : 00 00  00 78
0030   : 00 00  00 00
0034   : 00 00  00 00
0038   : 00 00  00 00
003C   : 00 00  00 00
0040   : 00 00  CD DC
0044   : 00 00  00 DC
0048   : FF FF  CD DC
004C   : FF FF  FF DC
*/  

