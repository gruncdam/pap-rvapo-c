#include <stdint.h>
#define C_LW(addr,offset)		*((volatile uint32_t*)(addr+offset))
#define C_SW(addr,offset)		(*(volatile uint32_t*)(addr+offset))	
#define AUTOAXIANSWER			0x2BCDDCBA
int main(){
	int val1=0x12345678;
	int val2=0xABBACDDC;

	int axi_base=C_LW(C_LW(4,0),-4);
	C_SW(axi_base,4)=AUTOAXIANSWER;
	C_SW(axi_base,8)=AUTOAXIANSWER;

	asm volatile ( 
		"sw %0, 5(zero);"
		"sw %1, 11(zero);"
		"sh %1, 16(zero);"
		"sh %1, 21(zero);"	//0x15
		"sh %1, 27(zero);"	//0x1B
		"sb %1, 32(zero);"	//0x20
		"sb %1, 37(zero);"
		"sb %1, 42(zero);"
		"sb %1, 47(zero);"

		"lw s4, 8(%2);"
		"lw s2, 5(zero);"
		"lw s4, 8(%2);"
		"sw s2, 52(zero);"	//0x34
		"nop;"

		"lw s4, 8(%2);"
		"lhu s2, 16(zero);"
		"lw s4, 8(%2);"
		"sh s2, 60(zero);"
		"nop;"

		"lw s4, 8(%2);"
		"lhu s2, 21(zero);"
		"lw s4, 8(%2);"
		"sh s2, 64(zero);"	//0x40
		"nop;"

		"lw s4, 8(%2);"
		"lhu s2, 27(zero);"
		"lw s4, 8(%2);"
		"sh s2, 68(zero);"
		"nop;"

		"lw s4, 8(%2);"
		"lbu s2, 32(zero);"
		"lw s4, 8(%2);"
		"sb s2, 72(zero);"
		"nop;"

		"lbu s2, 37(zero);"
		"sb s2, 76(zero);"
		"nop;"

		"lbu s2, 42(zero);"
		"sb s2, 80(zero);"
		"nop;"

		"lbu s2, 47(zero);"
		"sb s2, 84(zero);"
		"nop;"

		"lw s2, 0x6(%2);"
		"sw s2,	88(zero);"

		"clz s0,s0;"

		"ebreak;"
			:
		    : "r" (val1),"r" (val2),"r" (axi_base)	//*/
		);
	}	
/*	Expected memory dump:
0000   : 00 00  10 00
0004   : 34 56  78 00
0008   : DC 00  00 12
000C   : 00 AB  BA CD
0010   : 00 00  CD DC
0014   : 00 CD  DC 00
0018   : DC 00  00 00
001C   : 00 00  00 CD
0020   : 00 00  00 DC
0024   : 00 00  DC 00
0028   : 00 DC  00 00
002C   : DC 00  00 00
0030   : 00 00  00 00
0034   : 12 34  56 78
0038   : 00 00  00 00
003C   : 00 00  CD DC
0040   : 00 00  CD DC
0044   : 00 00  CD DC
0048   : 00 00  00 DC
004C   : 00 00  00 DC
0050   : 00 00  00 DC
0054   : 00 00  00 DC
0058   : DC BA  2B CD
*/  

