
int maybe_multiply(int x,int times){
	if(times==0)return x;
    return x*maybe_multiply(x,times-1);
    }

int main(){
	int base=0x10000;
	int val=0xFFFFFFFF;
   // volatile int*rand=(int*)0x20;
   // volatile int d=maybe_multiply((*rand)+5,(*rand)+9);//15*(((*rand)&0xFF)+10);
   int d=maybe_multiply(5,1);
	asm volatile ( 
		"sw %0, 4(%1);"
		"sw %2, 0(%1);"
        "sw %2, 8(%1);"
		"sw %2, 8(%1);"
		"ebreak;"
			:
		    : "r" (val),"r" (base)	,"r" (d)	
		);
    }
