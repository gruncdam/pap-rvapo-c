#![no_std]
#![feature(asm)]
use core::arch::asm;

#[no_mangle]
pub extern "C" fn __ebreak() {
	unsafe {
		asm!("ebreak")
	}
}

#[no_mangle]
pub extern "C" fn __ecall() {
	unsafe {
		asm!("ecall")
	}
}

#[no_mangle]
pub unsafe extern "C" fn __set_simulation_debug_registers(a: usize, b: usize, c: usize, d: usize) {
	asm!(
		"mv t3, {0}",
		"mv t4, {1}",
		"mv t5, {2}",
		"mv t6, {3}",
		in(reg) a,
		in(reg) b,
		in(reg) c,
		in(reg) d
	);
}

pub unsafe fn set_simulation_debug_registers(a: impl ToRegValue, b: impl ToRegValue, c: impl ToRegValue, d: impl ToRegValue) {
	__set_simulation_debug_registers(
		a.to_reg_value(), b.to_reg_value(), c.to_reg_value(), d.to_reg_value()
	)
}

#[macro_export]
macro_rules! register_main {
	($main: ident) => {
		#[no_mangle]
		pub unsafe extern "C" fn _reset() -> ! {
			$main();
			
			$crate::halt()
		}

		#[no_mangle]
		#[link_section = ".reset_vector"]
		pub static _RESET_VECTOR: unsafe extern "C" fn() -> ! = _reset;

		#[panic_handler]
		fn panic_handler(info: &core::panic::PanicInfo<'_>) -> ! {
			$crate::halt_signal()
		}
	};
}

/// Tries to obfuscate the source of the value from the compiler so that its usages aren't optimized out.
pub fn black_box<T>(value: T) -> T {
	value // TODO
}

pub fn halt() -> ! {
	__ebreak();
	loop {}
}

pub fn halt_signal() -> ! {
	__ecall();
	loop {}
}

pub trait ToRegValue {
	fn to_reg_value(self) -> usize;
}
impl ToRegValue for usize {
	fn to_reg_value(self) -> usize {
		self
	}
}
impl ToRegValue for isize {
	fn to_reg_value(self) -> usize {
		usize::from_ne_bytes(
			isize::to_ne_bytes(self)
		)
	}
}
impl ToRegValue for u32 {
	fn to_reg_value(self) -> usize {
		self as usize
	}
}