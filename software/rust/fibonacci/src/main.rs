#![no_std]
#![no_main]

common::register_main!(main);

pub fn main() {
	let computed = fib(15);
	let expected: usize = 610;

	assert_eq!(computed, expected);
	unsafe {
		common::set_simulation_debug_registers(computed, computed, computed, expected);
	}
}

fn fib(n: usize) -> usize {
	match n {
		0 => 0,
		1 => 1,
		_ => fib(n - 1) + fib(n - 2)
	}
}
