#![no_std]
#![no_main]

common::register_main!(main);

static VEC_A: [i8; 32] = [
    1, -1, 127, -127,
    1, 2, 3, 4,
    -4, -3, -2, -1,
    56, 56, 32, 32,

    -56, -32, 56, 32,
    -56, -32, 56, 32,
    1, 0, 0, 0,
    0, 0, 3, 0,
];
static VEC_B: [i8; 32] = [
    -1, 1, -127, 127,
    -1, -2, -3, -4,
    1, 2, 3, 4,
    56, 56, 32, 32,

    -56, -32, 56, 32,
    56, 32, -56, -32,
    0, 2, 0, 0,
    0, 0, 0, 4,
];
// static VEC_OUT: [i8; 32] = [0, 0, 0, 0, 0, 0, 0, 0, -3, -1, 1, 3, 112, 112, 64, 64, -112, -64, 112, 64, 0, 0, 0, 0, 1, 2, 0, 0, 0, 0, 3, 4];

pub fn main() {
	let mut computed = [0i8; 32];
    for (i, (a, b)) in VEC_A.iter().zip(VEC_B).enumerate() {
        computed[i] = a + b;
    }

    let summed = computed.into_iter().map(isize::from).sum::<isize>();

	assert_eq!(computed[0], 0);
    assert_eq!(computed[12], 112);
    assert_eq!(computed[31], 4);
    assert_eq!(summed, 362);
	unsafe {
		common::set_simulation_debug_registers(
            computed[0] as isize,
            computed[12] as isize,
            computed[31] as isize,
            summed
        );
	}
}

