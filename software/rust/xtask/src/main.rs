use std::{fs::OpenOptions, io::{BufReader, Seek, SeekFrom, Write}, path::{Path, PathBuf}, process::{Command, Stdio}};

use cargo_metadata::Message;

use object::{Endianness, FileKind, elf::{self, FileHeader32}, read::elf::{FileHeader, ProgramHeader}};

const USAGE: &'static str =
r#"
Usage: cargo xtask COMMAND
Commands:
	build BIN	Builds binary named BIN and outputs .bin and .vhdl files.
	dump BIN [flags]	Runs objdump on the binary
"#;

fn main() -> anyhow::Result<()> {
	let mut args = std::env::args().skip(1);
	match args.next().as_deref() {
		Some("build") => main_build(args)?,
		Some("dump") => main_dump(args)?,
		_ => anyhow::bail!(USAGE)
	};

	Ok(())
}

fn main_dump(mut args: impl Iterator<Item = String>) -> anyhow::Result<()> {
	let bin_name = match args.next() {
		Some(name) => name,
		None => anyhow::bail!(USAGE)
	};

	let rest_args = args;

	Command::new("cargo")
		.args([
			"objdump",
			"--release",
			"--target", "riscv32i-unknown-none-elf",
			"--bin", &bin_name,
			"--"
		])
		.args(rest_args)
		.status()?
	;

	Ok(())
}

fn main_build(mut args: impl Iterator<Item = String>) -> anyhow::Result<()> {
	let bin_name = match args.next() {
		Some(name) => name,
		None => anyhow::bail!(USAGE)
	};

	// Command::new("rm").args(["-rf", "out"]).status()?;
	Command::new("mkdir").args(["-p", "../out/vhdl"]).status()?;

	let elf_path = build_elf(&bin_name)?;
	let elf_data = std::fs::read(elf_path)?;
	let elf_segments = parse_elf(&elf_data)?;

/*	write_bin(
		&elf_segments,
		&PathBuf::from(format!("out/{}.bin", bin_name))
	)?;*/
	write_vhdl(
		&elf_segments,
		&PathBuf::from(format!("../out/vhdl/{}.vhd", bin_name))
	)?;
/*	write_mif(
		&elf_segments,
		&PathBuf::from(format!("out/{}.mif", bin_name))
	)?;*/

	Ok(())
}

fn build_elf(name: &str) -> anyhow::Result<PathBuf> {
	let mut command = Command::new("cargo")
        .args([
            "build",
            "--release",
			"--target", "riscv32i-unknown-none-elf",
            "--message-format", "json-render-diagnostics",
			"--bin", name
        ])
        .stdout(Stdio::piped())
        .spawn()?
	;

	let mut executable_artifact = None;

    let reader = BufReader::new(
		command.stdout.take().unwrap()
	);
    for message in Message::parse_stream(reader) {
        match message? {
            Message::CompilerMessage(msg) => {
                if let Some(rendered) = msg.message.rendered {
					println!("{}", rendered);
				}
            }
            Message::CompilerArtifact(artifact) if artifact.executable.is_some() => {
				if executable_artifact.is_some() {
					anyhow::bail!("Found multiple executable artifacts");
				}
				executable_artifact = Some(artifact);
            }
            _ => ()
        }
    }

	command.wait()?;

	match executable_artifact {
		None => Err(anyhow::anyhow!("Did not find any executable artifacts")),
		Some(artifact) => Ok(artifact.executable.unwrap().into_std_path_buf())
	}
}

struct ElfSegment<'data> {
	pub address: u64,
	pub data: &'data [u8]
}
impl std::fmt::Debug for ElfSegment<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Segment(addr = {}, len = {})", self.address, self.data.len())
    }
}

fn parse_elf<'data>(data: &'data [u8]) -> anyhow::Result<Vec<ElfSegment<'data>>> {
	match FileKind::parse(data)? {
		FileKind::Elf32 => (),
		kind => anyhow::bail!("Unexpected file kind: {:?}", kind)
	}

	let header = FileHeader32::<Endianness>::parse(data)?;
	let endian = header.endian()?;

	let mut segments = Vec::new();

	for segment in header.program_headers(endian, data)? {
		let physical_address: u64 = segment.p_paddr(endian).into();
		let segment_data = segment.data(endian, data).map_err(|_| anyhow::anyhow!("Failed to access segment data"))?;
		if !segment_data.is_empty() && segment.p_type(endian) == elf::PT_LOAD {
			segments.push(
				ElfSegment {
					address: physical_address,
					data: segment_data
				}
			);
		}
	}

	segments.sort_unstable_by_key(|s| s.address);
	
	Ok(segments)
}

fn write_bin(segments: &[ElfSegment<'_>], path: &Path) -> anyhow::Result<()> {
	let mut file = OpenOptions::new().write(true).truncate(true).create(true).open(path)?;

	for segment in segments {
		file.seek(
			SeekFrom::Start(segment.address)
		)?;
		file.write_all(segment.data)?
	}
	
	Ok(())
}

#[allow(dead_code)]
mod words {
	pub struct VhdlWordGenerator<const WORD_LEN: usize> {
		buffer: [u8; WORD_LEN],
		len: usize
	}
	impl<const WORD_LEN: usize> VhdlWordGenerator<WORD_LEN> {
		pub fn new() -> Self {
			VhdlWordGenerator {
				buffer: [0u8; WORD_LEN],
				len: 0
			}
		}

		fn push_byte(&mut self, byte: u8) -> Option<[u8; WORD_LEN]> {
			self.buffer[self.len] = byte;

			if self.len + 1 == self.buffer.len() {
				self.len = 0;
				Some(self.buffer)
			} else {
				self.len += 1;
				None
			}
		}

		pub fn generate<'i, I: Iterator<Item = u8> + 'i>(&'i mut self, mut iter: I) -> impl Iterator<Item = [u8; WORD_LEN]> + 'i {
			std::iter::from_fn(
				move || {
					while let Some(next) = iter.next() {
						if let Some(word) = self.push_byte(next) {
							return Some(word)
						}
					}

					None
				}
			)
		}

		/// Pads current buffer and returns it and number of bytes paddded.
		///
		/// Returns `None` if the current buffer is empty.
		pub fn pad(&mut self, pad_byte: u8) -> Option<([u8; WORD_LEN], usize)> {
			if self.len > 0 {
				let pad_len = self.buffer.len() - self.len;
				let word = self.generate(
					std::iter::repeat(pad_byte).take(pad_len)
				).next().unwrap();

				Some((word, pad_len))
			} else {
				None
			}
		}
	}

	fn fmt_word(mut writer: impl std::io::Write, word: [u8; 4]) -> anyhow::Result<()> {
		writeln!(writer, "		x\"{:0>8X}\",", u32::from_ne_bytes(word))?;

		Ok(())
	}
}

fn fmt_byte(mut writer: impl Write, address: usize, byte: u8) -> anyhow::Result<()> {
	write!(writer, "x\"{:0>2X}\",", byte)?;
	if address % 4 == 3 {
		writeln!(writer)?;
	}

	Ok(())
}

fn write_vhdl(segments: &[ElfSegment<'_>], path: &Path) -> anyhow::Result<()> {
	let mut file = OpenOptions::new().write(true).truncate(true).create(true).open(path)?;

	write!(
		&mut file,
r#"library IEEE;
use IEEE.std_logic_1164.all;
use WORK.rvapo_pkg.all;

package rvapo_cm_target_init is
	subtype Byte_t is std_logic_vector(7 downto 0);
	type memory_array_t is array (0 to 65535 + 4) of Byte_t;
	constant MEMORY_INIT: memory_array_t := (
"#
	)?;

	let mut address: usize = 0;
	for segment in segments {
		eprintln!("{:?}", segment);

		// pad with zeroes since last segment
		let pad_len = (segment.address as usize - address) as usize;
		for (i, byte) in std::iter::repeat(0).take(pad_len).enumerate() {
			fmt_byte(&mut file, address + i, byte)?;
		}
		address += pad_len;
		debug_assert_eq!(address, segment.address as usize);

		// write segment
		for (i, byte) in segment.data.iter().copied().enumerate() {
			fmt_byte(&mut file, address + i, byte)?;
		}
		address += segment.data.len();
	}

	write!(
		&mut file,
r#"		others => x"00"
	);
end package rvapo_cm_target_init;
"#
	)?;

	eprintln!("Last address: {}", address);

	Ok(())
}

fn write_mif(segments: &[ElfSegment<'_>], path: &Path) -> anyhow::Result<()> {
	let mut file = OpenOptions::new().write(true).truncate(true).create(true).open(path)?;

	write!(
		&mut file,
r#"
DEPTH = 16384;
WIDTH = 32;
ADDRESS_RADIX = HEX;
DATA_RADIX = HEX;

CONTENT
BEGIN
"#
	)?;

	let mut word_generator = words::VhdlWordGenerator::new();
	for segment in segments {
		assert_eq!(segment.address % 4, 0);
		write!(&mut file, "{:X}:", segment.address / 4)?;

		for word in word_generator.generate(segment.data.into_iter().copied()) {
			write!(&mut file, " {:X}", u32::from_ne_bytes(word))?;
		}
		if let Some((word, _)) = word_generator.pad(0) {
			write!(&mut file, " {:X}", u32::from_ne_bytes(word))?;
		}

		write!(&mut file, ";\n")?;
	}
	write!(&mut file, "END;\n")?;

	Ok(())
}
